import sqlite3
connection = sqlite3.connect("database.db")
selection=connection.cursor()

#delete table to prevent adding repeatly

connection.execute('drop table customer')
connection.execute('drop table admin')
connection.execute('drop table trading')
connection.execute('drop table shipping_info')
connection.execute('drop table ship')
connection.execute('drop table container')
connection.execute('drop table item')


#table
sql_command="""
CREATE TABLE customer(
cust_ID varchar(8) PRIMARY KEY,
cust_fname text(20) NOT NULL,
cust_lname text(20) NOT NULL,
cust_phone integer(15) NOT NULL,
cust_address varchar(80) NOT NULL,
cust_email varchar(26) NOT NULL,
cust_company varchar(25));"""
selection.execute(sql_command)

sql_command="""
CREATE TABLE admin(
admin_ID varchar(9) PRIMARY KEY,
admin_PW varchar(12) NOT NULL,
admin_fname text(20) NOT NULL,
admin_lname text(20) NOT NULL,
admin_phone integer(15) NOT NULL,
admin_address varchar(80) NOT NULL,
admin_email varchar(26) NOT NULL,
login_time integer(1) NOT NULL DEFAULT(0));"""
selection.execute(sql_command)

sql_command="""
CREATE TABLE trading(
trade_ID varchar(7) PRIMARY KEY,
admin_ID varchar(9) REFERENCES admin(admin_ID),
cust_ID varchar(8) REFERENCES customer (cust_ID),
shipping_ID text(7) REFERENCES shipping_info (shipping_ID),
order_when varchar(18) NOT NULL,
order_price float(10) NOT NULL,
destination varchar(30) NOT NULL,
priority integer(1) NOT NULL);"""
selection.execute(sql_command)

sql_command="""
CREATE TABLE shipping_info(
shipping_ID text(7) PRIMARY KEY,
ship_ID varchar(4) REFERENCES ship(ship_ID),
depart_time varchar(18),
arrangement text NOT NULL DEFAULT('NONE'));"""
selection.execute(sql_command)

sql_command="""
CREATE TABLE ship(
ship_ID varchar(4) PRIMARY KEY,
ship_name text(15) NOT NULL,
ship_type integer NOT NULL,
weight_support float NOT NULL,
control_tower text(6) NOT NULL,
ship_dimension text NOT NULL,
internal_type text(6) NOT NULL,
reefer_place_top text DEFAULT('NONE'),
reefer_place_bottom text DEFAULT('NONE'));"""
selection.execute(sql_command)

sql_command="""
CREATE TABLE container(
container_ID varchar(8) PRIMARY KEY,
trade_ID varchar(7) REFERENCES trading(trade_ID),
item_ID varchar(6) REFERENCES item(item_ID),
container_type text(8) NOT NULL,
container_size text(9) NOT NULL,
payload float(10) NOT NULL,
amount integer(5) NOT NULL);"""
selection.execute(sql_command)

sql_command="""
CREATE TABLE item(
item_ID varchar(6) PRIMARY KEY,
item_name text(20) NOT NULL,
item_type text(15) NOT NULL,
secure_way integer(2) NOT NULL);"""
selection.execute(sql_command)


#data inserting


admin=[('A66617821', '123456789012', 'Kelvin', 'Ree', '652441661', 'Block B-11-7, Texas Road, Texas Resident, Aberdeen, UK.', 'kelvinree@email.com','0'),
('A57219839', 'ABCDEFGHIJ', 'Jeff', 'Albert', '853131367', 'Block B-6-6, Texas Road, Texas Resident, Aberdeen, UK.', 'JA_jeffalbert@email.com','0'),
('A41233992', '123456ABCDEF', 'Wilson', 'Malken', '24555116', 'Block D-1-4, Texas Road, Texas Resident, Aberdeen, UK.', 'wilson_malken@email.com','0')]
command="""insert into admin values("{a}","{b}","{c}","{d}",{e},"{f}","{g}","{h}")"""
for data in admin:
    sql_command=command.format(a=data[0],b=data[1],c=data[2],d=data[3],e=data[4],f=data[5],g=data[6],h=data[7])
    selection.execute(sql_command)
    
customer=[('G1234567', 'John', 'Berdson', '0182111306', '26, Verdana Road, Valley Resident, Maryland, US.', 'j_berdson@email.com','Ferris'),
('H8421666', 'Albert', 'Chan', '52061255312', '2A-18, Chang Jiang Road, Mi Xiang Resident, Guang Zhou, China.', 'iamalbert@email.com' , 'Lucky Sea'),
('H4275114', 'Chris', 'Ravier', '177635247', 'Block C-1-2C, Blessing Road, Assort Garden Apartment, Queensland, Australlia.', 'ragelikeravier@email.com', 'Carter')]
command="""insert into customer values("{a}","{b}","{c}",{d},"{e}","{f}","{g}")"""
for data in customer:
    sql_command=command.format(a=data[0],b=data[1],c=data[2],d=data[3],e=data[4],f=data[5],g=data[6],)
    selection.execute(sql_command)

trading=[('A000000', 'A41233992', 'H8421666', '2542800', '11-Jul-15 08:11:52', '7700.00', 'Shanghai, China', '2'),
('A000001', 'A57219839', 'G1234567', '7274221', '01-Aug-15 15:21:20', '16000.00', 'Queensland, Australlia', '1'),
('A000002', 'A41233992', 'H4275114', '2542800', '11-Jul-15 08:14:53', '10250.00', 'Shanghai, China', '2'),
('A000003', 'A41233992', 'H4275114', '2542800', '11-Jul-15 08:31:01', '4200.00', 'New York, US', '1')]
command="""insert into trading values("{a}","{b}","{c}",{d},"{e}",{f},"{g}",{h})"""
for data in trading:
    sql_command=command.format(a=data[0],b=data[1],c=data[2],d=data[3],e=data[4],f=data[5],g=data[6],h=data[7])
    selection.execute(sql_command)

ship=[('A248', 'Panamax Plus', '2', '40000', 'sturn', '17,9,6', 'wide', '0-0', '0-0'),
('G111', 'Torben Cargo', '4', '52000', 'center', '15,9,5', 'narrow', '0-0', '0-0')]
command="""insert into ship values("{a}","{b}",{c},{d},"{e}","{f}","{g}","{h}","{i}")"""
for data in ship:
    sql_command=command.format(a=data[0],b=data[1],c=data[2],d=data[3],e=data[4],f=data[5],g=data[6],h=data[7],i=data[8])
    selection.execute(sql_command)

shipping_info=[('2542800', 'A248', '03-Dec-15 12:00:00', 'NONE'),
('7274221', 'G111', '04-Dec-15 10:45:00', 'NONE')]
command="""insert into shipping_info values("{a}","{b}","{c}","{d}")"""
for data in shipping_info:
    sql_command=command.format(a=data[0],b=data[1],c=data[2],d=data[3])
    selection.execute(sql_command)

connection.commit()
connection.close()

Hello, Linux users.

######################################## TAKE NOTE ##########################################

 The 'Decryptor.py' script should be obtained by the system administrator / super user ONLY.
 It is located inside ".../application/'linux platform'/Decryptor (.py)" directory.

#############################################################################################

Please copy the directory that located at .../application/'linux platform' from 
the disc or any portable to your desired location on your computer.

(a) In order to run the script, you should change the permission of the script.

	<RUN BY USING TERMINAL>
	1.	chmod +x /this_is_the_script_path/'Container Loading System.py'
	2.	python3 /this_is_the_script_path/'Container Loading System.py'
	
	<RUN BY USING GRAPHICAL USER INTERFACE>
	1.	Open Nautilus.
	2.	Navigate to the target file or folder.
	3.	Right click the file or folder.
	4.	Select Properties.
	5.	Click on the Permissions tab.
	6.	Click on the Access files in the Others section.
	7.	Select “Create and delete files”
	8.	Click Change Permissions for Enclosed Files.
	9.	Tick executable.
	10.	Confirm / save.
	11.	Double click on 'Container Loading System.py' to run.

(b) For those system that cannot run this application with "module not found" problem,
	1.	open terminal
	2. 	type "pip3 install --upgrade pip && pip install pyaes reportlab" into your terminal.
	3.	wait for downloading the python3 extra libraries.
	4.	after finish, go to "(a)"

import sqlite3
connection = sqlite3.connect("database.db")
selection=connection.cursor()

container=[('A248100A', 'A000000', 'A00015', 'dry', '20', '28590.00', '170'),
('A248301A', 'A000002', 'A00003', 'reefer', '20', '27500.00', '100'),
('G111401A', 'A000001', 'A00124', 'tank', '20', '26290.00', '280'),
('A248100C', 'A000000', 'A00011', 'dry', '40', '28590.00', '450'),
('A248304B', 'A000003', 'A00002', 'reefer', '40', '29400.00', '36'),
('A248302A', 'A000003', 'A00004', 'reefer', '20', '27500.00', '48'),
('A248303A', 'A000002', 'A00001', 'reefer', '20', '26000.00', '58'),
('G248402A', 'A000001', 'A00112', 'tank', '20', '25040.00', '74')]
command="""insert into container values("{a}","{b}","{c}","{d}",{e},{f},"{g}")"""
for data in container:
    sql_command=command.format(a=data[0],b=data[1],c=data[2],d=data[3],e=data[4],f=data[5],g=data[6])
    selection.execute(sql_command)

item=[('A00003', 'chicken', 'frozen', '2'),
('A00004', 'lamb', 'frozen', '2'),
('A00001', 'fruit', 'cold', '1'),
('A00002', 'vegetable', 'cold', '1'),
('A00124', 'lng', 'flamable', '3'),
('A00015', 'junk', 'dry', '0'),
('A00011', 'funiture', 'dry', '0'),
('A00112', 'chemical', 'gas(poison)', '4')]
command="""insert into item values("{a}","{b}","{c}",{d})"""
for data in item:
    sql_command=command.format(a=data[0],b=data[1],c=data[2],d=data[3])
    selection.execute(sql_command)

connection.commit()
connection.close()

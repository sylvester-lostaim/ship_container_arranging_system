# ship_container_arranging_system
#artificial #intelligence #ai #algorithm #arrange #ship #container

>>> The AI algorithm : arrange_system()

>>> linux user should add '#!/usr/bin/env python3' at the start of source codes

This is an AI system that will suggest the container arranging
picture on ship, where some rules like 20' or 40', dry or reefer
types are still being adhered. It is just being taken as AI logic
implementation, but cannot be fully reliable in real world.

This system also contains One-Time Password (OTP) functions to
fulfil the one of the objectives in this degree 3rd semester project.
The system may produce encrypted text, and can only be decrypted
by using decryptor.

All the modules and objects in the code had been removed and
compiled into "Workshop (Main).py" file, it might produce a lot
of bugs, but I'm not going to fix it. However, if curiosity does
not stop you, please explore on AI algorithm.

This is free software; see source for copying conditions. There is no warranty; not even for MERCHANTABILIY or FITNESS FOR A PARTICULAR PURPOSE.

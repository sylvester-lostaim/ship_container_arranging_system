import pyaes,string,codecs
from tkinter.messagebox import showinfo

try:
    email_file=open('Passcode (Request).txt',mode='r',encoding='utf-8')
    file_contain=email_file.read()
    secret_key=file_contain[len(file_contain)-64:]
    key=''
    for count in range(16):
                key=key+secret_key[2+count*4:4+count*4]
    key=key.encode('utf-8')
    encrypted=file_contain[:len(file_contain)-64].encode('utf-8')
    try:
        encrypted=codecs.escape_decode(encrypted)[0]
        converter=pyaes.AESModeOfOperationCTR(key)
        decrypted = converter.decrypt(encrypted).decode('utf-8')
        email_file=open('Passcode (Reply).txt',mode='w',encoding='utf-8')
        email_file.write(decrypted)
        email_file.close()
        showinfo(title="Message",message="'Passcode (Reply)' text file had been generated.\n\nPlease reply to your user by sending this file together.\n(The user must be TRUSTED only.)")
    except:
        showinfo(title="Message",message="The message in file is not in format.\n\nDecrypting process had been stopped.")
except:
    showinfo(title="Message",message="'Passcode (Request)' text file cannot be found\nin same (this application) folder.")

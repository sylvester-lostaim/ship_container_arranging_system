import sys
from cx_Freeze import setup, Executable

setup(name='Container Loading System',
      version='1.0',
      description="""Software Name: Container Loading System\nVersion 1.0\n\n@Workshop 1\n@Degree in Computer Science\n@Universiti Teknikal Malaysia Melaka\n@in 2015\n\n@A Simple Freeware (without evaluation)\n@Developed by SLA, student\n\nWe will not be responsible for any loss or damage that caused by this software.""",
      author="SLA",
      executables=[Executable(script="Workshop (Decryptor).py",
                              base="Win32GUI",
                              targetName="Decryptor.exe")])

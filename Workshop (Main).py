import sqlite3,time,pyaes,os,random,string
from tkinter import *
from tkinter.messagebox import *
from copy import deepcopy
from math import ceil
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch
from reportlab.lib.pagesizes import A4
from reportlab.lib.pagesizes import landscape
from reportlab.graphics import renderPDF
from reportlab.graphics.shapes import Drawing
from reportlab.graphics.charts.barcharts import VerticalBarChart

connection=sqlite3.connect("database.db")
database=connection.cursor()    # sqlite3_pointer
tk=Tk()     # tkinter_pointer
tk.title("Container Loading System")    # window configuration
tk.minsize(width=732,height=535)
tk.config(bg="grey98")

# GUI global data
global frame,frame2,frame3,admin_frame,admin_window,ship_window,search_window,report_window,back_button,next_button,extender,datetime,filter_word,loaddata_page,user_id,window,enter_key,admin_data,ship_data,ship_state,admin_state,exit_state,directory,windows
choiceA,choiceB,choiceC,workspace_tag,trading_current,container_current,workspace,addstate,loadstate,data_shippinginfo,data_tradinginfo,data_customer,data_container,data_item,data_current,Input,Input_clicked,Input_get,page_contain,search_contain,state,page,search_window,search_result,report_window,report_result,menu_list=IntVar(tk),IntVar(tk),IntVar(tk),[],0,0,[0,0],[False,False],False,[],[],[],[],[],[],[],[],[],[],[],True,[0,0],None,None,None,None,('New Data','Load Data','Log Out','Quit')
search_list=[('ID', 'Ship', 'Shipping Date'),('ID','Admin ID','Customer ID','Shipping ID','Order Date&Time','Destination'),('ID', 'Name', 'Type (number)', 'Internal Type', 'Max Weight Support', 'Control Tower Position'),('ID','Name','Phone no.','Address','Email','Company'),('ID', 'Trade ID','Item ID','Total Amount Sent'),('ID', 'Name', 'Type', 'Securing Way (number)'),('ID', 'Name', 'Phone no.','Address','Email')]
data_list=[['Shipping Info','Trading Info','Customer Info','Container Info','Item Info'],[['Shipping ID','Shipping Date','Shipping Time','Ship ID'],['Trading ID','Order Date','Order Time','Order Price','Destination','Unloading Priority'],['Customer ID','First Name','Last Name','Phone no','Email','Company Name','Address',' ',' ',' '],['Container ID','Container Type','Container Size','Total Payload','Amount'],['Item ID','Item Name','Item Type','Securing Way']],[0,4,10,20,25,29]]
database_command=(('shipping_info','trading','ship','customer','container','item','admin'),(('shipping_ID','ship_ID','depart_time'),('trade_ID','admin_ID','cust_ID','shipping_ID','order_when','order_price','destination'),('ship_ID','ship_name','ship_type','internal_type','weight_support','control_tower'),('cust_ID','cust_fname||\' \'||cust_lname','cust_phone','cust_address','cust_email','cust_company'),('container_ID','trade_ID','item_ID','amount'),('item_ID','item_name','item_type','secure_way'),('admin_ID','admin_fname||\" \"||admin_lname','admin_phone','admin_address','admin_email')),('Shipping_ID,Ship_ID,depart_time','*','ship_ID,ship_name,ship_type,weight_support,control_tower,internal_type,reefer_place_top,reefer_place_bottom','cust_ID, cust_fname,cust_lname,cust_phone,cust_email,cust_company,cust_address','*','*','admin_ID,admin_fname,admin_lname,admin_phone,admin_email,admin_address'))
data_entry_control=((7,9,8,4,7,9,8,10,30,1,8,20,20,15,26,25,20,20,20,20,8,8,2,10,5,6,20,15,1),(1,4,5,3,3,4,5,6,0,1,3,2,2,1,0,0,0,0,0,0,3,7,1,6,1,3,7,7,1))
instruction=(1234567,'dd-Jan-yy','HH:MM:SS (24-hour)','Ab12','ABab123','dd-Jan-yy','HH:MM:SS (24-hour)','1234567.89','ABCabc123,.',1,'ABCabc12','ABCabc','ABCabc',123,'ABCabc123.@','ABCabc123','ABCabc123,.','','','','ABCabc12','abcd1234',20,'1234567.89',12345,'ABab12','abc123','abc123',0)
choiceA.set(-1)

# Interfaces and child_windows
def login_interface():
        tk.attributes('-topmost',True)
        global frame,frame2,exit_state
        frame=Frame(tk,bg='light grey')
        frame.pack(side="top",fill="both",expand=True,padx=4,pady=4)
        a=('Menu','Arrange...','Ship Data','Delete...')
        for i in range(len(a)):
                menu_bar.entryconfig(a[i],state="disabled")
        Button(frame,relief=FLAT,bg='light grey',font="Arial 8",text="<< More",command=system_admin_pass).pack(anchor=NW)
        frame2=Frame(frame,bg="light grey")
        frame2.pack(expand=True)
        Label(frame2,text=" [ Container Loading System ]",font="Arial 13 bold",fg="black",bg="light grey",justify=CENTER).grid(column=0,row=0,columnspan=3,ipady=8)
        Label(frame2,text="  Username :",justify=RIGHT,font="Arial 11",fg="black",bg="light grey").grid(column=0,row=1,sticky=E)
        Label(frame2,text="  Password :",justify=RIGHT,font="Arial 11",fg="black",bg="light grey").grid(column=0,row=2,sticky=E)
        b=("username","password")
        for a in range(len(b)):
                newinput(a,True)
                Input[a].insert(0,b[a])
        Input[1].config(show="*")
        Button(frame2,text="Sign In",fg="black",command=login).grid(row=3,column=1,sticky=E,ipady=1,ipadx=14,padx=6,pady=4)
        Button(frame2,text="exit",fg="black",command=exiting).grid(row=3,column=0,sticky=W,padx=13,ipadx=3)
        Label(frame,text="< Please contact your system administrator for any further help > ",bg="dark grey",fg="black",font="Arial 8 italic").pack(anchor=SE,ipadx=4)
        exit_state=True

def working_interface():
        tk.attributes('-topmost',False)
        menu_bar.entryconfig('Menu',state="normal")
        menu_bar.entryconfig('Ship Data',state="normal")
        menu_bar.entryconfig('Delete...',state="normal")
        global frame,frame2,choiceB,extender,datetime,filter_word
        choiceB.set(0)
        frame=Frame(tk,relief=SOLID,borderwidth=0.5)#report frame
        frame.grid(column=0,row=0,columnspan=2,pady=6,padx=8,ipadx=2,sticky=NW)
        Label(frame,text="REPORT",font="Arial 10 bold").grid(column=0,row=0,padx=2)
        Label(frame,text="     By Year :",font="Arial 10").grid(column=1,row=0)
        validation=(frame.register(checkinput),'%d','%s','%S',2,1,None)
        year=Entry(frame,validate="key",validatecommand=validation)
        year.grid(column=2,row=0)
        Button(frame,text="Generate",command=lambda: report_year(year)).grid(column=3,row=0,padx=2,pady=5)
        Label(frame,text="\tBy Month :",font="Arial 10").grid(column=4,row=0)
        month=Entry(frame,validate="key",validatecommand=validation)
        month.grid(column=5,row=0)
        Button(frame,text="Generate",command=lambda: report_month(year,month)).grid(column=6,row=0,padx=2,pady=5)
        extender=Label(frame,font="Arial 8")
        extender.grid(column=7,row=0,ipadx=2)
        datetime=Label(frame,font="Arial 8")
        datetime.grid(column=8,row=0)
        realtime()
        frame2=Frame(tk,relief=SOLID,borderwidth=0.5)#searching frame
        frame2.grid(column=0,row=1,padx=8,sticky=NW)
        Label(frame2,font="Arial 10 bold",text="SEARCH\n\nCategory :\t\t\t").grid(column=0,row=0,sticky=W)
        a=["Shipping Info   ","Trading Info   ","Ship Info   ","Customer   ","Container   ","Item Info   ","Admin"]
        for b in range(len(a)):
                Radiobutton(frame2,font="Arial 10",text=a[b],variable=choiceB,value=b,command=searching_choice_handler).grid(column=0,row=1+b,sticky=W)
        Button(frame2,text="Retrieve All >>",justify=LEFT,font="Arial 8",command=lambda: generate_search(True)).grid(column=0,row=8,sticky=W,padx=4)
        Label(frame2,font="Arial 10 bold",fg="black",text="\nFilter :").grid(column=0,row=9,sticky=W)
        validation=(frame.register(checkinput),'%d','%s','%S',80,0,None)
        filter_word=Entry(frame2,validate="key",validatecommand=validation)
        filter_word.grid(column=0,row=10,sticky=W,padx=4)
        Button(frame2,text=" → ",font="Times 7",command=lambda: generate_search(False)).grid(column=0,row=10,sticky=E,padx=14,ipadx=5)
        for b in range(0,6,1):
                Label(frame2,font="Arial 10").grid(ipady=2,row=11+b,column=0)
        searching_choice_handler()

def newdata_interface():
        global frame3,back_button,next_button,page_contain,Input,data_entry_control,instruction,workspace_tag,state,enter_key,directory
        build_frame(True)
        Label(frame3,text=" New Data  \n",bg="white",font="Times 20 bold").grid(row=0,column=0,sticky=W)
        Label(frame3,text="(Press 'Ctrl+w' to close this panel.)",font="Arial 7",bg="white").grid(row=0,column=1,columnspan=2,sticky=NE)
        Button(frame3,text="x",fg="white",bg="dark grey",command=destroy_frame).grid(column=3,row=0,sticky=NE,ipadx=10,padx=8)
        enter_key=Label(frame3,text=" < Enter >",fg="black",bg="white")
        enter_key.grid(column=3,row=1,sticky=W)
        for a in range(0,10,1):
                page_contain.append(Label(frame3,bg="white",font="Arial 11"))
                page_contain[a].grid(column=1,row=1+a,sticky=E,pady=1)
        for a in range(0,29,1):
                newinput(a,False)
                Input[a].insert(0,instruction[a])
        Input[10].bind("<Return>",find_customer)
        Input[25].bind("<Return>",find_item)
        a=False
        while(a==False):
                b=(''.join(random.choice(string.digits) for i in range(7)))
                database.execute("select * from shipping_info where shipping_ID=?",(a,))
                if(database.fetchall()==[]):
                        a=True
                        Input[0].delete(0,END)
                        Input[0].insert(0,int(b))
                        Input[0].config(state="disabled")
        a=[[False,False],[4,20],[7,8],["select * from trading where trade_ID=?","select * from container where container_ID=?"]]
        for c in range(2):
                while(a[0][c]==False):
                        b=(''.join(random.choice(string.ascii_letters+string.digits) for i in range(a[2][c])))
                        database.execute(a[3][c],(b,))
                        if(database.fetchall()==[]):
                                a[0][c]=True
                                Input[a[1][c]].delete(0,END)
                                Input[a[1][c]].insert(0,b)
                                Input[a[1][c]].config(state="disabled")
        Label(frame3,bg="white").grid(column=1,row=0,ipadx=80)
        Label(frame3,bg="white").grid(column=2,row=0,ipadx=70)
        back_button=Button(frame3,text=" ← Back ",font="Times 10",command=minuspage)
        back_button.grid(row=11,column=2,sticky=E,pady=20)
        next_button=Button(frame3,text=" Next → ",font="Times 10",command=addpage)
        next_button.grid(row=11,column=3,sticky=W,pady=20,padx=5)
        for a in range(0,7,1):
                Label(frame3,bg="white").grid(pady=1,column=0,row=9+a)
        workspace=Frame(frame3,bg='white')
        workspace.grid(column=0,row=12,rowspan=4,columnspan=4)        
        Label(workspace,bg='white',font='Arial 9 bold',justify=LEFT,text='  Trading & Container Info').grid(column=0,row=0,columnspan=7)
        Label(workspace,bg='white',fg='white',font='Arial 9',justify=LEFT,text='#########').grid(column=3,row=2,sticky=W)
        workspace_tag.append(Label(workspace,bg='white',text='0 / 0',font='Arial 9'))
        workspace_tag[0].grid(column=5,row=1)
        workspace_tag.append(Label(workspace,bg='white',text='0 / 0',font='Arial 9'))
        workspace_tag[1].grid(column=5,row=2)
        Label(workspace,bg='white',font='Arial 9',justify=LEFT,text='  Trading ID : ').grid(column=2,row=1,sticky=E)
        Label(workspace,bg='white',font='Arial 9',justify=LEFT,text='  Container ID : ').grid(column=2,row=2,sticky=E)
        workspace_tag.append(Button(workspace,text='<',font='Arial 6',state="disabled",command=lambda:workspace_state(2)))
        workspace_tag[2].grid(column=4,row=1,sticky=W,pady=1,padx=5,ipadx=3)
        workspace_tag.append(Button(workspace,text='>',font='Arial 6',state="disabled",command=lambda:workspace_state(3)))
        workspace_tag[3].grid(column=6,row=1,sticky=W,pady=1+a,padx=5,ipadx=3)
        workspace_tag.append(Button(workspace,font='Verdana 7',text=' Add ',state="disabled",command=lambda:workspace_state(4)))
        workspace_tag[4].grid(column=0,row=1,padx=5)
        workspace_tag.append(Button(workspace,font='Verdana 7',text=' Del ',state="disabled",command=delete_trading))
        workspace_tag[5].grid(column=1,row=1,padx=5,ipadx=1,pady=5)
        workspace_tag.append(Button(workspace,text='<',font='Arial 6',state="disabled",command=lambda:workspace_state(6)))
        workspace_tag[6].grid(column=4,row=2,sticky=W,pady=1,padx=5,ipadx=3)
        workspace_tag.append(Button(workspace,text='>',font='Arial 6',state="disabled",command=lambda:workspace_state(7)))
        workspace_tag[7].grid(column=6,row=2,sticky=W,pady=1+a,padx=5,ipadx=3)
        workspace_tag.append(Button(workspace,font='Verdana 7',text=' Add ',state="disabled",command=lambda:workspace_state(8)))
        workspace_tag[8].grid(column=0,row=2,padx=5)
        workspace_tag.append(Button(workspace,font='Verdana 7',text=' Del ',state="disabled",command=delete_container))
        workspace_tag[9].grid(column=1,row=2,padx=5,ipadx=1,pady=5)
        workspace_tag.append(Label(workspace,bg='white',font='Arial 9',justify=LEFT,text='-'))
        workspace_tag[10].grid(column=3,row=1,sticky=W)
        workspace_tag.append(Label(workspace,bg='white',font='Arial 9',justify=LEFT,text='-'))
        workspace_tag[11].grid(column=3,row=2,sticky=W)
        state=True
        directory=Label(frame3,font='Arial 9',bg='white',justify=LEFT)
        directory.grid(row=11,column=0,sticky=W,columnspan=3)
        page_handler()

def loaddata_interface():
        global frame3,choiceA,back_button,next_button,loaddata_page,user_id
        build_frame(False)
        Label(frame3,bg="white").grid(ipadx=1,column=0,row=0)
        Label(frame3,text="Load Data",bg="white",font="Times 20 bold").grid(row=0,column=1,sticky=W)
        Label(frame3,text="(Press 'Ctrl+w' to close this panel.)",font="Arial 7",bg="white").grid(row=0,column=1,sticky=NE)
        Button(frame3,text="x",fg="white",bg="dark grey",command=destroy_frame).grid(column=2,row=0,sticky=NE,ipadx=10,padx=8)
        Label(frame3,font="Arial 12 bold",bg="black",fg="white",text=" Shipping ID          Ship ID          Departure Time                           No of Customer          No of Container          Admin          ").grid(column=1,row=1,sticky=NW,pady=6)
        database.execute("""select a.shipping_ID,b.ship_ID,b.depart_time,count(distinct a.cust_ID),sum(c.amount),a.admin_ID from trading a,shipping_info b,container c where a.shipping_ID=b.shipping_ID and a.trade_ID=c.trade_ID group by a.shipping_ID order by b.depart_time desc""")
        temp,d,e=database.fetchall(),"",[21,15,35,30,29,10]
        a=sorted(temp,key=lambda i: time.strptime(i[2], '%d-%b-%y %H:%M:%S'),reverse=True)
        for b in range(0,11,1):
                Label(frame3,bg="white").grid(column=0,row=2+b,pady=5)
        b=Label(frame3,bg="white")
        b.grid(column=0,row=14,columnspan=4,rowspan=2,pady=14)
        back_button=Button(b,text=" ← Back ",font="Times 10",bg="white")
        back_button.pack(side=LEFT)
        validation=(b.register(checkinput),'%d','%s','%S',3,1,None)
        loaddata_page=Entry(b,width=4,bg="gray85",justify=CENTER,font="Arial 10 bold",validate="key",validatecommand=validation)
        loaddata_page.pack(side=LEFT,padx=20)
        loaddata_page.insert(0,"1")
        next_button=Button(b,text=" Next → ",font="Times 10")
        next_button.pack(side=LEFT)
        for b in range(len(a)):
                for c in range(len(a[b])):
                        d=d+" "+str(a[b][c]).ljust(e[c])
                Label(frame3,font="Verdana 10",text='a',bg="light grey",fg="light grey").grid(row=2+b,column=1,pady=5,sticky=W,ipadx=425)
                Label(frame3,font="Verdana 10",text=d,bg="light grey",fg="black").grid(row=2+b,column=1,pady=5,sticky=W)
                Radiobutton(frame3,indicatoron=0,text="Proceed >>",fg="black",variable=choiceA,value=b,command=lambda:database_handler(a[choiceA.get()][0])).grid(row=2+b,column=2,sticky=E)
                d=""
        Label(frame3,bg="white").grid(row=1,column=3)

#GUI handlers
def full_reset(a):
        global Input,Input_clicked,Input_get,page_contain,search_contain,state,page,choiceA,extender,user_id,data_shippinginfo,data_tradinginfo,data_customer,data_container,data_item,data_current,loadstate,trading_current,container_current,workspace_tag,workspace,addstate
        workspace_tag,trading_current,container_current,workspace,data_shippinginfo,data_tradinginfo,data_customer,data_container,data_item,data_current,Input,Input_clicked,Input_get,page_contain,state,page,loadstate,addstate=[],0,0,[0,0],[],[],[],[],[],[],[],[],[],[],True,[0,0],False,[False,False]
        choiceA.set(-1)
        tk.bind_all("<Control-w>",'break')
        c=('New Data','Load Data')
        for d in c:
                menu_button.entryconfig(d,state="normal")
        try:
                frame3.grid_forget()
                extender.grid_forget()
                tk.bind_all("<Control-n>",lambda event: newdata_interface())
                tk.bind_all("<Control-l>",lambda event: loaddata_interface())
        except:
                pass
        menu_bar.entryconfig('Arrange...',state="disabled")
        if(a==True):
                tk.bind_all("<Control-n>",'break')
                tk.bind_all("<Control-l>",'break')
                tk.bind_all("<Control-g>",'break')
                b=('Menu','Arrange...','Ship Data','Delete...')
                for i in range(len(b)):
                        menu_bar.entryconfig(b[i],state="disabled")
                search_contain,filter_word,year,month,user_id=[],None,None,None,None
                frame2.grid_forget()
                frame.grid_forget()
        
def login():
        global Input,Input_clicked,user_id
        if((Input[1].get()=='')or(Input[0].get()=='')):
                showinfo(title='Message',message="All entries must be filled.")
        else:
                database.execute("select admin_ID,login_time from admin where admin_PW=?",(Input[1].get(),))
                a=database.fetchone()
                if ((a==None) or (a[0]!=Input[0].get())):
                        database.execute("select login_time from admin where admin_ID=?",(Input[0].get(),))
                        a=database.fetchone()
                        if(a==None):
                                showerror(title="Access Denied",message="Wrong ID and Password")
                        elif(a[0]<4):
                                database.execute("update admin set login_time=? where admin_ID=?",(a[0]+1,Input[0].get(),))
                                connection.commit()
                                showerror(title="Access Denied",message="Wrong ID and Password")
                        else:
                                askSomething()
                else:
                        if(a[1]>3):
                                askSomething()
                        else:
                                user_id=Input[0].get()
                                Input=[]
                                Input_clicked=[]
                                frame2.pack_forget()
                                frame.pack_forget()
                                tk.bind_all("<Control-n>",lambda event:newdata_interface())
                                tk.bind_all("<Control-l>",lambda event:loaddata_interface())
                                tk.bind_all("<Control-g>",lambda event:logout())
                                working_interface()

def askSomething():       
        a=(''.join(random.choice(string.ascii_letters + string.digits + '!@#$%^&*()') for i in range(32)))
        b=a.encode('utf-8')
        c=pyaes.AESModeOfOperationCTR(b)
        d=(''.join(random.choice(string.ascii_letters + string.digits + '!@#$%^&*()') for i in range(20)))
        e=str(c.encrypt(d))
        e=e[2:len(str(e))-1]
        a=str(a)
        for count in range(16):
                a=a[:0+count*4]+'\\x'+a[0+count*4:]
        email_file=open('Passcode (Request).txt',mode='w',encoding='utf-8')
        email_file.write(e+a)
        del a
        email_file.close()
        login_error=Toplevel(tk)
        login_error.title("Reset Password")
        login_error.attributes('-topmost',True)
        login_error.grab_set()
        login_error.resizable(False,False)
        error_frame=Frame(login_error)
        error_frame.pack(fill=BOTH,expand=True)
        Label(error_frame,text="\n<<<Instruction>>>\nPlease send the 'Passcode (Request)' text file\nto your system administrator to obtain\nthe VALID passcode.\n\n(The file is located at the same location with this software.)").grid(column=0,row=0,columnspan=2)
        Label(error_frame,text="Passcode : ").grid(row=1,column=0,sticky=E)
        Label(error_frame,text="Create New Password : ").grid(row=2,column=0,sticky=E)
        Label(error_frame,text="Re-enter Password : ").grid(row=3,column=0,sticky=E)
        validation=(error_frame.register(checkinput),'%d','%s','%S',20,0,None)
        passcode=Entry(error_frame,validate="key",validatecommand=validation)
        passcode.grid(row=1,column=1,sticky=W)
        validation=(error_frame.register(checkinput),'%d','%s','%S',12,0,None)
        password_1=Entry(error_frame,show="*",validate="key",validatecommand=validation)
        password_1.grid(row=2,column=1,sticky=W)
        password_2=Entry(error_frame,show="*",validate="key",validatecommand=validation)
        password_2.grid(row=3,column=1,sticky=W)
        Button(error_frame,text="Confirm",command=lambda: update_admin_info(login_error,passcode,password_1,password_2,d)).grid(row=5,column=0,columnspan=2,pady=3)
        Label(error_frame,text="*The passcode will be changed after closing this window.",font="Arial 8 italic").grid(row=4,column=0,columnspan=2,pady=1)
        
def update_admin_info(login_error,passcode,password_1,password_2,a):
        global Input
        if(passcode.get()!=a):
                showerror(title="Wrong Passcode",message="Wrong Passcode had been detected.\n\nNew passcode had been regenerated.\nPlease resend the file to your system administrator.")
                login_error.destroy()
                askSomething()
        else:
                if(password_1.get()!=password_2.get()):
                        showwarning(title="Message",message="The passwords in both entries do\nnot match each other.")
                        password_1.delete(0,END)
                        password_2.delete(0,END)
                else:
                        database.execute("update admin set login_time=?, admin_PW=? where admin_ID=?",(0,password_2.get(),Input[0].get(),))
                        connection.commit()
                        login_error.destroy()
                        showinfo(title='Message',message="New password had been implemented successfully.")

def system_admin_pass():
        global admin_window,admin_frame
        a=(''.join(random.choice(string.ascii_letters + string.digits + '!@#$%^&*()') for i in range(32)))
        b=a.encode('utf-8')
        c=pyaes.AESModeOfOperationCTR(b)
        d=(''.join(random.choice(string.ascii_letters + string.digits + '!@#$%^&*()') for i in range(20)))
        e=str(c.encrypt(d))
        e=e[2:len(str(e))-1]
        a=str(a)
        for count in range(16):
                a=a[:0+count*4]+'\\x'+a[0+count*4:]
        email_file=open('Passcode (Request).txt',mode='w',encoding='utf-8')
        email_file.write(e+a)
        del a
        email_file.close()
        admin_window=Toplevel(tk)
        admin_window.title("System Administrator Tool")
        admin_window.attributes('-topmost',True)
        admin_window.grab_set()
        admin_window.resizable(False,False)
        admin_frame=Frame(admin_window)
        admin_frame.grid()
        Label(admin_frame,text="New passcode had been generated in 'Passcode (Request)' text file\nin same (this application) folder.").grid(column=0,row=0,columnspan=2)
        Label(admin_frame,text="Please enter the passcode: ").grid(column=0,row=1,sticky=E)
        a=Entry(admin_frame)
        a.grid(column=1,row=1,sticky=W,pady=2)
        Button(admin_frame,text='Enter',command=lambda:check_admin(a,d)).grid(column=0,row=2,pady=4,ipadx=3,columnspan=2)

def check_admin(a,d):
        if(d!=a.get()):
                showinfo(title="Wrong Passcode",message="Wrong Passcode")
                admin_window.destroy()
                return None
        else:
                system_admin_tool()
                
def system_admin_tool():
        global admin_window,admin_frame
        admin_frame.grid_forget()
        admin_frame=Frame(admin_window)
        admin_frame.grid()
        Button(admin_frame,text=' Add Admin ',bg='light grey',relief=GROOVE,command=add_admin).pack(side=LEFT,padx=2,pady=2)
        Button(admin_frame,text=' Edit Admin ',bg='light grey',relief=GROOVE,command=edit_admin).pack(side=LEFT,padx=2,pady=2)
        Button(admin_frame,text='Delete Admin',bg='light grey',relief=GROOVE,command=del_admin).pack(side=LEFT,padx=2,pady=2)

def add_admin():
        global admin_frame,admin_data,admin_state
        admin_state=True
        admin_frame.grid_forget()
        admin_frame=Frame(admin_window)
        admin_frame.grid()
        admin_data=[]
        a=(('Admin ID','Password','Reenter Password','First Name','Last Name','Phone','Address','Email'),(9,12,12,20,20,15,80,26),(3,0,0,2,2,1,0,0))
        for i in range(len(a[0])):
                validation=(frame2.register(checkinput),'%d','%s','%S',a[1][i],a[2][i],None)
                Label(admin_frame,text=a[0][i]+" : ",font='Arial 11').grid(column=0,row=0+i,sticky=E)
                admin_data.append(Entry(admin_frame,width=50,font="Arial 10",validate="key",validatecommand=validation))
                admin_data[i].grid(column=1,row=0+i,sticky=W,padx=3)
        admin_data[1].config(show='*')
        admin_data[2].config(show='*')
        Button(admin_frame,text="Save",command=save_admin).grid(column=1,row=9,sticky=E,pady=4,ipadx=4,padx=3)
        Button(admin_frame,text="Back",command=system_admin_tool).grid(column=0,row=9,sticky=W,pady=4,ipadx=4,padx=3)

def edit_admin():
        global admin_frame,admin_window
        admin_frame.grid_forget()
        admin_frame=Frame(admin_window)
        admin_frame.grid()
        Label(admin_frame,text="Which admin data is going to be edited?",font="Arial 11").grid(column=0,row=0,columnspan=2)
        Label(admin_frame,text="Admin ID :",font="Arial 11").grid(column=0,row=1,sticky=E)
        validation=(frame2.register(checkinput),'%d','%s','%S',9,3,None)
        a=(Entry(admin_frame,font="Arial 10",validate="key",validatecommand=validation))
        a.grid(column=1,row=1)
        Button(admin_frame,text="Confirm",command=lambda:admin_exist(a)).grid(column=1,row=2,sticky=E,ipadx=2,pady=4,padx=3)
        Button(admin_frame,text="Back",command=system_admin_tool).grid(column=0,row=2,sticky=W,ipadx=2,pady=4,padx=3)
        
def del_admin():
        global admin_frame,admin_window
        admin_frame.grid_forget()
        admin_frame=Frame(admin_window)
        admin_frame.grid()
        Label(admin_frame,text="Which admin data is going to be deleted?",font="Arial 11").grid(column=0,row=0,columnspan=2)
        Label(admin_frame,text="Admin ID :",font="Arial 11").grid(column=0,row=1,sticky=E)
        validation=(frame2.register(checkinput),'%d','%s','%S',9,3,None)
        del_admin_id=(Entry(admin_frame,font="Arial 10",validate="key",validatecommand=validation))
        del_admin_id.grid(column=1,row=1)
        Button(admin_frame,text="Confirm",command=lambda:remove_admin(del_admin_id)).grid(column=1,row=2,sticky=E,ipadx=2,pady=4,padx=3)
        Button(admin_frame,text="Back",command=system_admin_tool).grid(column=0,row=2,sticky=W,ipadx=2,pady=4,padx=3)

def admin_exist(a):
        global admin_data,admin_state
        if(a.get()==''):
                showinfo(title="Message",message="Please fill up the entry.")
                return None
        database.execute("select * from admin where admin_ID=?",(a.get(),))
        b=database.fetchall()
        if(b==[]):
                showinfo(title="Message",message="This admin ID does not exist.")
                return None
        else:
                add_admin()
                admin_state=False
                admin_data[0].insert(0,b[0][0])
                for c in range(3,8,1):
                        admin_data[c].insert(0,b[0][c-1])
                admin_data[0].config(state="disabled")

def save_admin():
        global admin_data
        for a in range(len(admin_data)):
                if(len(admin_data[a].get())==0):
                        showinfo(title='Message',message="Please fill up all the fields")
                        return None
                if((a==0)and(len(admin_data[0].get())!=9)):
                        showinfo(title='Message',message="ID length should be in 9 characters.")
                        return None
                if(a==0):
                        database.execute("select * from admin where admin_ID=?",(admin_data[0].get,))
                        if((database.fetchall()!=[])and(admin_state==True)):
                                showinfo(title="Message",message="This admin ID had been used before.\nPlease use another self-created ID.")
                                return None
                if((a==1)and(len(admin_data[1].get())<8)):
                        showinfo(title='Message',message="Minimum password length is 8 characters.")
                        return None
                if((a==2)and(admin_data[2].get()!=admin_data[1].get())):
                        showinfo(title="Message",message="Both passwords does not match each other.")
                        return None
        command="insert or replace into admin(admin_ID,admin_PW,admin_fname,admin_lname,admin_phone,admin_address,admin_email,login_time) values('{a}','{b}','{c}','{d}',{e},'{f}','{g}',{h})"
        sql_command=command.format(a=admin_data[0].get(),b=admin_data[1].get(),c=admin_data[3].get(),d=admin_data[4].get(),e=admin_data[5].get(),f=admin_data[6].get(),g=admin_data[7].get(),h=0)
        database.execute(sql_command)
        connection.commit()
        showinfo(title='Message',message="Current admin data had been saved.")
        system_admin_tool()
                
def remove_admin(a):
        if(a.get()==''):
                showinfo(title="Message",message="Please fill up the entry.")
                return None
        database.execute("select * from admin where admin_ID=?",(a.get(),))
        if(database.fetchall()==[]):
                showinfo(title="Message",message="This admin ID does not exist.")
                return None
        database.execute("select * from trading_info where admin_ID=?",(a.get(),))
        if(database.fetchall()!=[]):
                showinfo(title="Message",message="You are not allowed to delete this admin.\n(Some trades are related with this admin.)")
                return None
        if(askyesno(title="Message",message="Are you really want to delete this admin info?\n\n(The trading info(s) which having the correlation with\nthis admin will be auto-shifted to another admin)")==False):
                return None
        database.execute("delete from admin where admin_ID=?",(a.get(),))
        connection.commit()
        showinfo(title='Message',message="This admin data had been deleted.\n(Those trading ID had been replaced by using "+str(c[0][0])+", admin ID.)")
        system_admin_tool()
        
def realtime():
        global datetime
        datetime.config(text=(time.strftime('%H:%M:%S\n%d-%b-%y')))
        datetime.after(200,realtime)

def build_frame(a):
        tk.bind_all("<Control-n>",'break')
        tk.bind_all("<Control-l>",'break')
        tk.bind_all("<Control-w>",lambda event:destroy_frame())
        global frame,frame3,extender,exit_state
        exit_state=False
        if(a==True):
                extender=Label(frame,font="Arial 8")
                extender.grid(column=7,row=0,ipadx=2)
        else:
                extender=Label(frame,font="Arial 8")
                extender.grid(column=7,row=0,ipadx=218)
        frame3=Frame(tk,relief=SOLID,borderwidth=0.5,bg="white")
        frame3.grid(column=1,row=1,sticky=NW)
        b=('New Data','Load Data')
        for a in b:
                menu_button.entryconfig(a,state="disabled")
        
def destroy_frame():
        if(state==False):
                if((askokcancel(title="WARNING",message="Any unsaved data or changes will be LOST.\n\nAre you sure?"))==True):
                        full_reset(False)
        else:
                full_reset(False)

def logout():
        if(state==False):
                showinfo(title="Message",message="Please make sure there is no editing\nbefore leaving.")
        else:
                if(askyesno(title="Message",message="Logging out?")==True):
                        full_reset(True)
                        login_interface()

def exiting():
        if(state==False):
                if(askyesno(title="WARNING",message="Unsaved data and changes had been detected !\n\nExit without saving?")==True):
                        tk.destroy()
        else:
                if(askyesno(title="Message",message="Exiting?")==True):
                        tk.destroy()

def newship():
        global ship_window,ship_data,ship_state
        ship_state=True
        menu_bar.entryconfig('Ship Data',state="disabled")
        ship_data=[]
        ship_window=Toplevel(tk)
        ship_window.title("New Ship")
        ship_window.attributes('-topmost',True)
        ship_window.focus()
        ship_window.resizable(False,False)
        a=[['Ship ID','Ship Name','Ship Type','Total Weight Support (ton)','Control Tower Position','Internal Type','Bay for reefer on deck (from)','Bay for reefer on deck (to)','Bay for Reefer at internal (from)','Bay for Reefer at internal (to)','','Total Height','Internal Height','Total Width'],[4,15,1,8,6,6,2,2,2,2,0,2,2,2],[0,0,1,0,7,7,1,1,1,1,0,1,1,1]]
        for b in range(len(a[0])):
                if(b==10):
                        Label(ship_window,font='Arial 11',text='\n< Ship Dimension >').grid(column=0,row=0+b,sticky=W)
                        ship_data.append(Entry(ship_window))
                        ship_data[b].insert(0,'0')
                        ship_data[b].grid_forget()
                        continue
                validation=(ship_window.register(checkinput),'%d','%s','%S',a[1][b],a[2][b],None)
                Label(ship_window,font="Arial 11",text=a[0][b]+' : ').grid(column=0,row=0+b,sticky=E)
                ship_data.append(Entry(ship_window,validate="key",validatecommand=validation))
                ship_data[b].grid(column=1,row=0+b,sticky=W)
        Label(ship_window).grid(column=2,row=0)
        Button(ship_window,text='Save Data',font="Times 10",command=lambda:check_ship_data(ship_data)).grid(columnspan=2,pady=6)
        ship_window.wm_protocol('WM_DELETE_WINDOW',exit_ship)

def editship():
        menu_bar.entryconfig('Ship Data',state="disabled")
        global ship_window
        ship_window=Toplevel(tk)
        ship_window.title("Edit Ship")
        ship_window.attributes('-topmost',True)
        ship_window.focus()
        ship_window.resizable(False,False)
        Label(ship_window,text="Which ship data is going to be edited?",font="Arial 11").grid(column=0,row=0,columnspan=2)
        Label(ship_window,text="Ship ID :",font="Arial 11").grid(column=0,row=1,sticky=E)
        validation=(frame2.register(checkinput),'%d','%s','%S',4,3,None)
        a=(Entry(ship_window,font="Arial 10",validate="key",validatecommand=validation))
        a.grid(column=1,row=1)
        Button(ship_window,text="Confirm",command=lambda:ship_exist(a)).grid(column=1,row=2,sticky=E,ipadx=2,pady=4,padx=3)
        Button(ship_window,text="Close",command=close_ship).grid(column=0,row=2,sticky=W,ipadx=2,pady=4,padx=3)
        Label(ship_window).grid(column=2,row=0)

def ship_exist(a):
        global ship_data,ship_window,ship_state
        if(a.get()==""):
                showinfo(title="Message",message="Please fill up the entry.")
                return None
        database.execute("select ship_ID,ship_name,ship_type,weight_support,control_tower,internal_type,reefer_place_top,reefer_place_bottom,ship_dimension from ship where ship_ID=?",(a.get(),))
        b=database.fetchall()
        if(b==[]):
                showinfo(title="Message",message="This ship ID doesn't exist.")
                return None
        ship_window.destroy()
        newship()
        ship_window.title('Edit Ship')
        ship_state=False
        temp=list(b[0])
        for c in range(6):
                if((c==2)or(c==3)):
                        temp[c]=int(temp[c])
                ship_data[c].insert(0,temp[c])
        temp2=temp[6].split('-')
        for c in range(len(temp2)):
                ship_data[c+6].insert(0,int(temp2[c]))
        temp2=temp[7].split('-')
        for c in range(len(temp2)):
                ship_data[c+8].insert(0,int(temp2[c]))
        temp2=temp[8].split(',')
        for c in range(len(temp2)):
                ship_data[c+11].insert(0,int(temp2[c]))
        ship_data[0].config(state="disabled")

def check_ship_data(ship_data):
        global ship_window,ship_state
        for b in range(len(ship_data)):
                if(len(ship_data[b].get())==0):
                        showinfo(title='Message',message="Please fill up all entires.")
                        return None
                if(b==0):
                        if(len(ship_data[0].get())!=4):
                           showinfo(title='Message',message='Ship ID should be in 4 characters length')
                           return None
                        database.execute("select ship_ID from ship where ship_ID=?",(ship_data[0].get(),))
                        if((database.fetchall()!=[])and(ship_state==True)):
                                showinfo(title='Message',message='This ship_ID,'+str(ship_data[0].get())+' is already existed.')
                                return None
                if(b==4):
                        if((ship_data[4].get()=='sturn')or(ship_data[4].get()=='center')):
                                continue
                        else:
                                showinfo(title='Mesage',message="Control Tower Position should be 'sturn' or 'center' only.")
                                return None
                if(b==5):
                        if((ship_data[5].get()=='wide')or(ship_data[5].get()=='narrow')):
                                continue
                        else:
                                showinfo(title='Mesage',message="Internal Type should be 'wide' or 'narrow' only.")
                                return None
                if((b==6)or(b==8)):
                        if(int(ship_data[b].get())<1):
                                if(int(ship_data[b+1].get())>0):
                                        showinfo(title='Message',message="Smallest number of bay for reefer is 1 only\n\n(from '0' to '0' means no reefer place.)")
                                        return None
                if((b==7)or(b==9)):
                        if(int(ship_data[b].get())>16):
                                showinfo(title='Message',message='largest number of bay for reefer is 16 only')
                                return None
                if(b==7):
                        if(int(ship_data[6].get())>int(ship_data[7].get())):
                                if((ship_data[6].get()=='0')and(ship_data[7].get()=='0')):
                                        continue
                                else:
                                        showinfo(title='Message',message="Bay for reefer on deck\n\n'From' should be a smaller integer than 'to'")
                                        return None
                if(b==9):
                        if(int(ship_data[8].get())>int(ship_data[9].get())):
                                if((ship_data[8].get()=='0')and(ship_data[9].get()=='0')):
                                        continue
                                else:
                                        showinfo(title='Message',message="Bay for reefer at internal\n\n'From' should be a smaller integer than 'to'")
                                        return None
                        if((int(ship_data[8].get())<6)or(int(ship_data[9].get())>13)):
                                if((ship_data[8].get()=='0')and(ship_data[9].get()=='0')):
                                        continue
                                showinfo(title='Message',message="Bay for reefer at internal only from 6-13.")
                                return None
                if(b==12):
                        if(int(ship_data[12].get())>=int(ship_data[11].get())):
                                showinfo(title='Message',message="Total height should be larger than internal height.")
                                return None
        command="insert or replace into ship (ship_ID,ship_name,ship_type,weight_support,control_tower,internal_type,reefer_place_top,reefer_place_bottom,ship_dimension) values('{a}','{b}',{c},{d},'{e}','{f}','{g}','{h}','{i}')"
        sql_command=command.format(a=ship_data[0].get(),b=ship_data[1].get(),c=ship_data[2].get(),d=ship_data[3].get(),e=ship_data[4].get(),f=ship_data[5].get(),g=ship_data[6].get()+'-'+ship_data[7].get(),h=ship_data[8].get()+'-'+ship_data[9].get(),i=ship_data[11].get()+','+ship_data[12].get()+','+ship_data[13].get())
        database.execute(sql_command)
        connection.commit()
        showinfo(title="Message",message="Current ship data had been saved.")
        ship_window.destroy()
        menu_bar.entryconfig('Ship Data',state="normal")

def exit_ship():
        global ship_window
        if(askyesno(title="Message",message="Close without saving?")==True):
                ship_window.destroy()
                menu_bar.entryconfig('Ship Data',state="normal")

def deleteship():
        menu_bar.entryconfig('Ship Data',state="disabled")
        global ship_window
        ship_window=Toplevel(tk)
        ship_window.title("Delete Ship")
        ship_window.attributes('-topmost',True)
        ship_window.focus()
        ship_window.resizable(False,False)
        Label(ship_window,text="Which ship data is going to be deleted?",font="Arial 11").grid(column=0,row=0,columnspan=2)
        Label(ship_window,text="Ship ID :",font="Arial 11").grid(column=0,row=1,sticky=E)
        validation=(frame2.register(checkinput),'%d','%s','%S',4,3,None)
        del_ship_id=(Entry(ship_window,font="Arial 10",validate="key",validatecommand=validation))
        del_ship_id.grid(column=1,row=1)
        Button(ship_window,text="Confirm",command=lambda:remove_ship(del_ship_id)).grid(column=1,row=3,sticky=E,ipadx=2,pady=4,padx=3)
        Button(ship_window,text="Close",command=close_ship).grid(column=0,row=3,sticky=W,ipadx=2,pady=4,padx=3)
        Label(ship_window).grid(column=2,row=0)

def remove_ship(a):
        if(a.get()==''):
                showinfo(title="Message",message="Please fill up the entry.")
                return None
        database.execute("select * from ship where ship_ID=?",(a.get(),))
        h=database.fetchall()
        if(h==[]):
                showinfo(title="Message",message="The ship ID doesn't exist.")
                return None
        database.execute("select * from shipping_info where ship_ID=?",(a.get(),))
        s=database.fetchall()
        if(s!=[]):
                showinfo(title="Message",message="You cannot delete this ship info.\n\n(Some shipping info(s) are related to this ship.)")
                return None
        if(askyesno(title="Message",message="Are you sure?")==False):
                return None
        database.execute("delete from ship where ship_ID=?",(a.get(),))
        connection.commit()
        showinfo(title='Message',message="This ship data had been deleted.")
        ship_window.destroy()
        menu_bar.entryconfig('Ship Data',state="normal")

def close_ship():
        ship_window.destroy()
        menu_bar.entryconfig('Ship Data',state="normal")

def report_year(a):
        situation,situation2,year,data,data2,maxvalue,maxvalue2=False,False,a.get(),[],[],0,0
        if(a.get()==''):
                showinfo(title="Message",message="Please input year without century in 'year' field.\n\nEg: 98, 99, 00, 01, 02")
                return None
        if(len(a.get())==1):
                showinfo(title="Message",message="Please input year without century.\n\nEg: 98, 99, 00, 01, 02")
                return None
        b=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
        for c in range(len(b)):
                d='%'+b[c]+'-'+year+'%'
                database.execute("select sum(order_price) from trading where order_when like ?",(d,))
                e=database.fetchone()
                database.execute("select sum(container.amount) from container where trade_ID=(select trade_ID from trading where shipping_ID=(select shipping_ID from shipping_info where depart_time like ?))",(d,))
                f=database.fetchone()
                if(e[0]==None):
                        data.append(0)
                        
                else:
                        data.append(e[0])
                        situation=True
                        if(maxvalue<e[0]):
                                maxvalue=e[0]
                if(f[0]==None):
                        data2.append(0)
                else:
                        data2.append(f[0])
                        situation2=True
                        if(maxvalue2<f[0]):
                                maxvalue2=f[0]
        if((situation==False)and(situation2==False)):
                showinfo(title="Message",message="Currently, there is no data for this year.")
                return None
        filename="Annually Report in '"+year+".pdf"
        the_canvas=canvas.Canvas(filename,pagesize=landscape(A4))
        if(situation==True):
                temp=[]
                temp.append(data)
                drawing=Drawing(400, 200)
                bc=VerticalBarChart()
                bc.x=50
                bc.y=50
                bc.height=250
                bc.width=600
                bc.data=temp
                bc.valueAxis.valueMin=0
                bc.valueAxis.valueMax=maxvalue
                bc.valueAxis.valueStep=int(maxvalue/10)
                bc.categoryAxis.labels.boxAnchor='ne'
                bc.categoryAxis.labels.dx=8
                bc.categoryAxis.labels.dy=-2
                bc.categoryAxis.labels.angle=30
                bc.categoryAxis.categoryNames=b
                drawing.add(bc)
                title="Annually Report for Profit in '"+year
                the_canvas.setFont('Helvetica',40,leading=None)
                the_canvas.drawCentredString(415,500,title)
                the_canvas.translate(1*inch,2*inch)
                renderPDF.draw(drawing, the_canvas,0,0)
                the_canvas.showPage()
        else:
                showinfo(title="Message",message="There is no data for profit in this year.\n\n(No chart will be generated for profit.)")
        if(situation2==True):
                temp=[]
                temp.append(data2)
                drawing2=Drawing(400, 200)
                bc2=VerticalBarChart()
                bc2.x=50
                bc2.y=50
                bc2.height=250
                bc2.width=600
                bc2.data=temp
                bc2.valueAxis.valueMin=0
                bc2.valueAxis.valueMax=maxvalue2
                bc2.valueAxis.valueStep=int(maxvalue2/10)
                bc2.categoryAxis.labels.boxAnchor='ne'
                bc2.categoryAxis.labels.dx=8
                bc2.categoryAxis.labels.dy=-2
                bc2.categoryAxis.labels.angle=30
                bc2.categoryAxis.categoryNames=b
                drawing2.add(bc2)
                title="Annually Report for Container Sent in '"+year
                the_canvas.setFont('Helvetica',40,leading=None)
                the_canvas.drawCentredString(415,500,title)
                the_canvas.translate(1*inch,2*inch)
                renderPDF.draw(drawing2, the_canvas,0,0)
                the_canvas.showPage()
        else:
                showinfo(title="Message",message="There is no data for container sent in this year.\n\n(No chart will be generated for container sent.)")
        try:
                the_canvas.save()
                showinfo(title="Message",message="\""+filename+"\" had been generated in same application file.")
        except:
                showinfo(title="Message",message="System cannot overwrite the file for the "+filename+" is being used by other application.")

def report_month(a,b):
        situation,situation2,year,month,data,data2,maxvalue,maxvalue2=False,False,a.get(),b.get(),[],[],0,0
        if(year==''):
                showinfo(title="Message",message="Please input year without century in 'By Year' field.\n\nEg: 98, 99, 00, 01, 02")
                return None
        if(len(year)==1):
                showinfo(title="Message",message="Please input year without century.\n\nEg: 98, 99, 00, 01, 02")
                return None
        if((month=='')):
                showinfo(title="Message",message="Please input month in 'By Month' field.\n\nEg: 1, 2, 3, 4...")
                return None
        count=1
        date=str(count)+'-'+month+'-'+year
        c=time.strptime(date,'%d-%m-%y')
        from time import mktime
        from datetime import datetime
        d=datetime.fromtimestamp(mktime(c))
        nextmonth=deepcopy(d)
        import datetime
        try:
                nextmonth=nextmonth.replace(month=nextmonth.month+1)
        except ValueError:
                if nextmonth.month == 12:
                        nextmonth=nextmonth.replace(year=nextmonth.year+1, month=1)
        c=[]
        for i in range(31):
                c.append(d+datetime.timedelta(days=i))
                if(c[i]==nextmonth):
                        c.pop()
                        break;
                c[i]=str(c[i])[0:10]
                c[i]=time.strptime(c[i],'%Y-%m-%d')
                c[i]=time.strftime('%d-%b-%y',c[i])+'%'
                database.execute("select sum(order_price) from trading where order_when like ?",(c[i],))
                e=database.fetchone()
                database.execute("select sum(container.amount) from container where trade_ID=(select trade_ID from trading where shipping_ID=(select shipping_ID from shipping_info where depart_time like ?))",(c[i],))
                f=database.fetchone()
                if(e[0]==None):
                        data.append(0)
                        
                else:
                        data.append(e[0])
                        situation=True
                        if(maxvalue<e[0]):
                                maxvalue=e[0]
                if(f[0]==None):
                        data2.append(0)
                else:
                        data2.append(f[0])
                        situation2=True
                        if(maxvalue2<f[0]):
                                maxvalue2=f[0]
        if((situation==False)and(situation2==False)):
                showinfo(title="Message",message="Currently, there is no data for this month.")
                return None
        title="Monthly Report for Profit on "+(str(c[0]))[3:6]+" '"+(str(c[0]))[7:9]
        title2="Monthly Report for Container Sent on "+(str(c[0]))[3:6]+" '"+(str(c[0]))[7:9]
        filename="Monthly Report on "+(str(c[0]))[3:6]+" '"+(str(c[0]))[7:9]+".pdf"
        the_canvas=canvas.Canvas(filename,pagesize=landscape(A4))
        for i in range(len(c)):
                c[i]=str(c[i])[0:2]
        if(situation==True):
                temp=[]
                temp.append(data)
                drawing = Drawing(400, 200)
                bc=VerticalBarChart()
                bc.x=50
                bc.y=50
                bc.height=250
                bc.width=730
                bc.data=temp
                bc.valueAxis.valueMin=0
                bc.valueAxis.valueMax=maxvalue
                bc.valueAxis.valueStep=int(maxvalue/10)
                bc.categoryAxis.labels.boxAnchor='ne'
                bc.categoryAxis.labels.dx=3
                bc.categoryAxis.labels.dy=-2
                bc.categoryAxis.labels.angle=30
                bc.categoryAxis.categoryNames=c
                drawing.add(bc)
                the_canvas.setFont('Helvetica',40,leading=None)
                the_canvas.drawCentredString(415,500,title)
                the_canvas.translate(0*inch,2*inch)
                renderPDF.draw(drawing, the_canvas,0,0)
                the_canvas.showPage()
        else:
                showinfo(title="Message",message="There is no data for profit in this month.\n\n(No chart will be generated for profit.)")
        if(situation2==True):
                temp=[]
                temp.append(data2)
                drawing2=Drawing(400, 200)
                bc2=VerticalBarChart()
                bc2.x=50
                bc2.y=50
                bc2.height=250
                bc2.width=730
                bc2.data=temp
                bc2.valueAxis.valueMin=0
                bc2.valueAxis.valueMax=maxvalue2
                bc2.valueAxis.valueStep=int(maxvalue2/10)
                bc2.categoryAxis.labels.boxAnchor='ne'
                bc2.categoryAxis.labels.dx=3
                bc2.categoryAxis.labels.dy=-2
                bc2.categoryAxis.labels.angle=30
                bc2.categoryAxis.categoryNames=c
                drawing2.add(bc2)
                the_canvas.setFont('Helvetica',40,leading=None)
                the_canvas.drawCentredString(415,500,title2)
                the_canvas.translate(0*inch,2*inch)
                renderPDF.draw(drawing2, the_canvas,0,0)
                the_canvas.showPage()
        else:
                showinfo(title="Message",message="There is no data for container sent in this month.\n\n(No chart will be generated for container sent.)")
        try:
                the_canvas.save()
                showinfo(title="Message",message="\""+filename+"\" had been generated in same application file.")
        except:
                showinfo(title="Message",message="System cannot overwrite the file for the "+filename+" is being used by other application.")

def addpage():
        global page
        if(getUserinput(True)==True):
                page[0]=page[0]+1
                if(page[0]<5):
                        page_handler()
                else:
                        page[0]=4
                        saving()

def minuspage():
        global page
        if(getUserinput(False)==True):
                page[0]=page[0]-1
                page_handler()

def page_handler():
        global page,frame3,page_contain,back_button,next_button,data_list,Input,Input_clicked,enter_key,directory
        if(page[0]==0):
                back_button.config(state="disabled")
                directory.config(text='  { Current Directory }\n  SHIPPING   > Trading > Customer > Container > Item (Save)')
        elif(page[0]==4):
                next_button.config(text="< Save >")
                directory.config(text='  { Current Directory }\n  Shipping > Trading > Customer > Container >   ITEM (SAVE)  ')
        else:
                back_button.config(state="normal")
                next_button.config(text=" Next → ")
        if(page[0]==1):
                directory.config(text='  { Current Directory }\n  Shipping >   TRADING   > Customer > Container > Item (Save) ')
        elif(page[0]==2):
                directory.config(text='  { Current Directory }\n  Shipping > Trading >   CUSTOMER   > Container > Item (Save) ')
        elif(page[0]==3):
                directory.config(text='  { Current Directory }\n  Shipping > Trading > Customer >   CONTAINER   > Item (Save) ')
        if((page[0]==2)or(page[0]==4)):
                enter_key.config(fg="black")
        else:
                enter_key.config(fg="white")
        for a in range(len(page_contain)):
                page_contain[a].config(text="")
        if(type(page[1])==int):
                page[1]=Label(frame3,font="Arial 11 bold")
                page[1].grid(column=0,row=1,sticky=W)
        page[1].config(justify="left",text="   [ "+data_list[0][page[0]]+" ]",bg="white")
        for a in range(len(data_list[1][page[0]])):
                if(a<7):
                        page_contain[a].config(text=data_list[1][page[0]][a]+" :")
                else:
                        page_contain[a].config(text=data_list[1][page[0]][a])
                Input[a+data_list[2][page[0]]].grid(column=2,row=1+a)

def find_customer(event):
        database.execute("select cust_fname,cust_lname,cust_phone,cust_email,cust_company,cust_address from customer where cust_ID=?",(Input[10].get(),))
        a=database.fetchall()
        if(a!=[]):
                if(askyesno(title="Customer Found",message="Same Customer ID was found.\nDo you want to insert same data for the rest?")==False):
                        return None
                c=[]
                for b in a[0]:
                        c.append(b)
                for b in range(0,4,1):
                        try:
                                c.insert(5+b,(c[5+b])[0+20*b:20+20*b])
                        except:
                                c.insert(5+b,(""))
                for b in range(11,20,1):
                        Input[b].delete(0,END)
                        Input[b].insert(0,c[b-11])
        else:
                if(askyesno(title="Message",message="No record for '"+str(Input[10].get())+"'\nAre you going to generate a new ID?")==True):
                        z=False
                        while(z==False):
                                y=(''.join(random.choice(string.ascii_letters+string.digits) for i in range(8)))
                                database.execute("select * from customer where cust_ID=?",(y,))
                                if(database.fetchall()==[]):
                                        z=True
                                        Input[10].delete(0,END)
                                        Input[10].insert(0,y)
                else:
                        Input[10].delete(0,END)
                        Input[10].insert(0,Input_get[10])
                
def find_item(event):
        database.execute("select item_name,item_type,secure_way from item where item_ID=?",(Input[25].get(),))
        a=database.fetchall()
        if(a!=[]):
                if(askyesno(title="Item Found",message="Same Item ID was found.\nDo you want to insert same data for the rest?")==False):
                       return None
                for b in range(26,29,1):
                        Input[b].delete(0,END)
                        Input[b].insert(0,a[0][b-26])
        else:
                if(askyesno(title="Message",message="No record for '"+str(Input[25].get())+"'\nNew random ID had been generated.")==True):
                        z=False
                        while(z==False):
                                y=(''.join(random.choice(string.ascii_letters+string.digits) for i in range(6)))
                                database.execute("select * from item where item_ID=?",(y,))
                                if(database.fetchall()==[]):
                                        z=True
                                        Input[25].delete(0,END)
                                        Input[25].insert(0,y)
                else:
                        Input[25].delete(0,END)
                        Input[25].insert(0,Input_get[25])

def add_info(a):
        global Input,Input_get,Input_clicked,frame3,instruction,addstate,loadstate,workspace_tag,trading_current
        Label(frame3,text=" Add Data\n",bg="white",font="Times 20 bold").grid(row=0,column=0,sticky=W)
        Input[20].config(state="normal")
        c=False
        while(c==False):
                b=(''.join(random.choice(string.ascii_letters+string.digits) for i in range(8)))
                database.execute("select * from container where container_ID=?",(b,))
                if(database.fetchall()==[]):
                        c=True
                        Input[20].delete(0,END)
                        Input[20].insert(0,b)
                        Input[20].config(state="disabled")
        workspace_tag[11].config(text='-')
        
        for b in range(2,10,1):
                workspace_tag[b].config(state="disabled")
        if(a==4):
                Input[4].config(state="normal")
                workspace_tag[0].config(text=str(len(data_current)+1)+' / '+str(len(data_current)+1))
                workspace_tag[1].config(text='1 / 1')
                workspace_tag[10].config(text='-')
                workspace_tag[5].config(state='normal')
                for b in range(1,4,1):
                        Input[b].config(state="disabled")
                for b in range(4,29,1):
                        Input_get[b]=''
                        Input_clicked[b]=False
                        Input[b].delete(0,END)
                        Input[b].insert(0,instruction[b])
                c=False
                while(c==False):
                        workspace_tag[1].config(text=str(data_current[trading_current]+1)+' / '+str(data_current[trading_current]+1))
                        b=(''.join(random.choice(string.ascii_letters+string.digits) for i in range(7)))
                        database.execute("select * from trading where trade_ID=?",(b,))
                        if(database.fetchall()==[]):
                                c=True
                                Input[4].delete(0,END)
                                Input[4].insert(0,b)
                                Input[4].config(state="disabled")
                addstate[1]=True
        else:
                workspace_tag[1].config(text=str(data_current[trading_current]+1)+' / '+str(data_current[trading_current]+1))
                workspace_tag[9].config(state='normal')
                for b in range(1,20,1):
                        Input[b].config(state="disabled")
                for b in range(20,29,1):
                        Input_get[b]=''
                        Input_clicked[b]=False
                        Input[b].delete(0,END)
                        Input[b].insert(0,instruction[b])
        loadstate,addstate[0]=False,True

def database_handler(a):
        full_reset(False)
        global frame3,data_shippinginfo,data_tradinginfo,data_customer,data_container,data_item,data_current,loadstate
        temp=[]
        database.execute("select shipping_ID,depart_time,ship_ID from shipping_info where shipping_ID=?",(a,))
        data_shippinginfo=database.fetchone()
        database.execute("select cust_ID,trade_ID,order_when,order_price,destination,priority from trading where shipping_ID=? order by trade_ID",(a,))
        data_tradinginfo=database.fetchall()
        for a in range(len(data_tradinginfo)):
                database.execute("select cust_ID,cust_fname,cust_lname,cust_phone,cust_email,cust_company,cust_address from customer where cust_ID=?",(data_tradinginfo[a][0],))
                data_customer.append(database.fetchone())
                database.execute("select item_ID,container_ID,container_type,container_size,payload,amount from container where trade_ID=?",(data_tradinginfo[a][1],))
                temp.append(database.fetchall())
                data_current.append(len(temp[a]))
                for b in range(len(temp[a])):
                        database.execute("select item_ID,item_name,item_type,secure_way from item where item_ID=?",(temp[a][b][0],))
                        data_item.append(database.fetchone())
        for a in range(len(data_current)):
                for b in range(data_current[a]):
                        data_container.append(temp[a][b])
        newdata_interface()
        loadstate=True
        menu_bar.entryconfig('Arrange...',state="normal")
        del temp
        change_workspace()

def workspace_state(a):
        global trading_current,container_current,workspace,data_current,state
        if(state==True):
                if((a==4)or(a==8)):
                        add_info(a)
                        return None
                b=0
                if(a==3):
                        trading_current,container_current,workspace[0]=trading_current+1,0,workspace[0]+1
                        for a in range(trading_current):
                                b=b+data_current[a]
                        workspace[1]=b
                elif(a==2):
                        trading_current,container_current,workspace[0]=trading_current-1,0,workspace[0]-1
                        for a in range(trading_current):
                                b=b+data_current[a]
                        workspace[1]=b
                elif(a==7):
                        container_current,workspace[1]=container_current+1,workspace[1]+1
                elif(a==6):
                        container_current,workspace[1]=container_current-1,workspace[1]-1
                change_workspace()
        else:
                showinfo(title="Message",message="Please click 'Next' until last page to save \nbefore changing current workspace.")
        
def change_workspace():
        global data_shippinginfo,data_tradinginfo,data_customer,data_container,data_item,data_current,Input_get,workspace,trading_current,container_current,workspace_tag,state,Input_clicked
        Input_get=[]
        Label(frame3,text=" Load Data \n",bg="white",font="Times 20 bold").grid(row=0,column=0,sticky=W)
        for a in data_shippinginfo:
                Input_get.append(a)
        for a in range(1,6,1):
                Input_get.append(data_tradinginfo[workspace[0]][a])
        for a in range(0,7,1):
                Input_get.append(data_customer[workspace[0]][a])
        for a in range(1,6,1):
                Input_get.append(data_container[workspace[1]][a])
        for a in data_item[workspace[1]]:
                Input_get.append(a)
        Input_get.insert(2,(Input_get[1])[10:])
        Input_get[1]=(Input_get[1])[:9]
        Input_get.insert(6,(Input_get[5])[10:])
        Input_get[5]=(Input_get[5])[:9]
        Input_get[9]=int(Input_get[9])
        for a in range(0,4,1):
                try:
                        Input_get.insert(16+a,(Input_get[16+a])[0+20*a:20+20*a])
                except:
                        Input_get.insert(16+a,(""))
        del Input_get[20]
        b=(0,4,20)
        for a in b:
                Input[a].config(state="normal")
        for a in range(0,29,1):
                Input_clicked[a]=False
                Input[a].delete(0,END)
                Input[a].insert(0,Input_get[a])
        for a in b:
                Input[a].config(state="disabled")
        workspace_tag[0].config(text=str(trading_current+1)+' / '+str(len(data_current)))
        workspace_tag[1].config(text=str(container_current+1)+' / '+str(data_current[trading_current]))
        if((trading_current+1)==1):
                if(len(data_current)==1):
                        workspace_tag[2].config(state="disabled")
                        workspace_tag[3].config(state="disabled")
                else:
                        workspace_tag[2].config(state="disabled")
                        workspace_tag[3].config(state="normal")
        elif((trading_current+1)==len(data_current)):
                workspace_tag[2].config(state="normal")
                workspace_tag[3].config(state="disabled")
        else:
                workspace_tag[2].config(state="normal")
                workspace_tag[3].config(state="normal")
        if((container_current+1)==1):
                if((data_current[trading_current])==1):
                        workspace_tag[6].config(state="disabled")
                        workspace_tag[7].config(state="disabled")
                else:
                        workspace_tag[6].config(state="disabled")
                        workspace_tag[7].config(state="normal")
        elif((container_current+1)==(data_current[trading_current])):
                workspace_tag[6].config(state="normal")
                workspace_tag[7].config(state="disabled")
        else:
                workspace_tag[6].config(state="normal")
                workspace_tag[7].config(state="normal")
        for a in range(2):
                workspace_tag[4+4*a].config(state="normal")
                workspace_tag[5+4*a].config(state="normal")
        workspace_tag[10].config(text=data_tradinginfo[workspace[0]][1])
        workspace_tag[11].config(text=data_container[workspace[1]][1])
        state=True

def saving():
        global data_shippinginfo,data_tradinginfo,data_customer,data_container,data_item,data_current,Input_get,workspace,data_current,state,user_id,loadstate
        if(askyesno(title="Save?",message="Are you going to save?")==False):
                return None
        temp=deepcopy(Input_get)
        temp[1]=temp[1]+' '+temp[2]
        temp[5]=temp[5]+' '+temp[6]
        temp[16]=temp[16]+temp[17]+temp[18]+temp[19]
        b=[2,5,15,15,15]
        for a in b:
                del temp[a]
        if((loadstate==False)and(addstate[0]==False)):
                data_shippinginfo,data_tradinginfo,data_customer,data_container,data_item=("","",""),[("","","","","","")],[(temp[8],'','','','','','')],[('','','','','','')],[(temp[20],'','','')]
        if((addstate[0]==True)and(addstate[1]==True)):
                data_tradinginfo.append(("","","","","",""))
                data_customer.append((temp[8],'','','','','',''))
                workspace[0],workspace[1]=workspace[0]+1,workspace[1]+1
                data_current.append(0)
        if(addstate[0]==True):
                data_container.append(('','','','','',''))
                data_item.append((temp[20],'','',''))
                data_current[workspace[0]]=data_current[workspace[0]]+1
        temp2=["","",""]
        for a in range(3):
                temp2[a]=temp[a]
        data_shippinginfo=tuple(temp2)
        temp2=["","","","","",""]
        for a in range(1,6,1):
                temp2[a]=temp[2+a]        
        temp2[0]=data_customer[workspace[0]][0]
        data_tradinginfo.insert(workspace[0]+1,tuple(temp2))
        del data_tradinginfo[workspace[0]]
        temp2=['','','','','','','']
        for a in range(7):
                temp2[a]=temp[8+a]
        data_customer.insert(workspace[0]+1,tuple(temp2))
        del data_customer[workspace[0]]
        temp2=['','','','','','']
        for a in range(1,6,1):
                temp2[a]=temp[14+a]
        temp2[0]=data_item[workspace[1]][0]
        data_container.insert(workspace[1]+1,tuple(temp2))
        del data_container[workspace[1]]        
        temp2=['','','','']
        for a in range(4):
                temp2[a]=temp[20+a]
        data_item.insert(workspace[1]+1,tuple(temp2))
        del data_item[workspace[1]]
        command="insert or replace into shipping_info (shipping_ID,depart_time,ship_ID) values('{a}','{b}','{c}')"
        sql_command=command.format(a=data_shippinginfo[0],b=data_shippinginfo[1],c=data_shippinginfo[2])
        database.execute(sql_command)
        command="insert or replace into trading (trade_ID,order_when,order_price,destination,priority,shipping_ID,admin_ID,cust_ID) values('{d}','{e}',{f},'{g}',{h},'{a}','{b}','{c}')"
        sql_command=command.format(a=data_shippinginfo[0],b=user_id,c=data_tradinginfo[workspace[0]][0],d=data_tradinginfo[workspace[0]][1],e=data_tradinginfo[workspace[0]][2],f=data_tradinginfo[workspace[0]][3],g=data_tradinginfo[workspace[0]][4],h=data_tradinginfo[workspace[0]][5])
        database.execute(sql_command)
        command="insert or replace into customer (cust_ID,cust_fname,cust_lname,cust_phone,cust_email,cust_company,cust_address) values('{a}','{b}','{c}',{d},'{e}','{f}','{g}')"
        sql_command=command.format(a=data_customer[workspace[0]][0],b=data_customer[workspace[0]][1],c=data_customer[workspace[0]][2],d=data_customer[workspace[0]][3],e=data_customer[workspace[0]][4],f=data_customer[workspace[0]][5],g=data_customer[workspace[0]][6])
        database.execute(sql_command)
        command="insert or replace into container (container_ID,container_type,container_size,payload,amount,trade_ID,item_ID) values('{b}','{c}','{d}',{e},{f},'{g}','{a}')"
        sql_command=command.format(a=data_container[workspace[1]][0],b=data_container[workspace[1]][1],c=data_container[workspace[1]][2],d=data_container[workspace[1]][3],e=data_container[workspace[1]][4],f=data_container[workspace[1]][5],g=data_tradinginfo[workspace[0]][1])
        database.execute(sql_command)
        command="insert or replace into item (item_ID,item_name,item_type,secure_way) values ('{a}','{b}','{c}','{d}')"
        sql_command=command.format(a=data_item[workspace[1]][0],b=data_item[workspace[1]][1],c=data_item[workspace[1]][2],d=data_item[workspace[1]][3])
        database.execute(sql_command)
        connection.commit()
        state=True
        if(loadstate==False):                
                database_handler(data_shippinginfo[0])

def delete_trading():
        global data_tradinginfo,data_shippinginfo,addstate
        if(addstate[0]==True):
                if(askyesno(title='Delete?',message="Not to save this newly created Trading Info?")==True):
                        database_handler(data_shippinginfo[0])
                return None
        if(len(data_tradinginfo)!=1):
                if(askyesno(title="Delete?",message="Delete the Trading Info where ID named "+str(data_tradinginfo[workspace[0]][1])+"?\nAll related container info(s) will be deleted as well.\n\nAre you sure?\n(This process is irreversible.)")==False):
                        return None
        else:
                if(askyesno(title="Delete?",message="This Trading info where ID named, "+str(data_tradinginfo[workspace[0]][1])+" is the only one in this shipping record.\nThis Shipping Record will also be removed for no trading info with it.\n\nAre you sure?\n(This process is irreversible.)")==False):
                        return None
        database.execute("delete from trading where trade_ID=?",(data_tradinginfo[workspace[0]][1],))
        database.execute("delete from container where trade_ID=?",(data_tradinginfo[workspace[0]][1],))
        if(len(data_tradinginfo)==1):
                database.execute("delete from shipping_info where shipping_ID=?",(data_shippinginfo[0],))
                connection.commit()
                full_reset(False)
        else:
                connection.commit()
                database_handler(data_shippinginfo[0])

def delete_container():
        global data_tradinginfo,data_container,data_shippinginfo,data_current,trading_current
        if(addstate[0]==True):
                if(askyesno(title='Delete?',message="Not to save this newly created Container Info?")==True):
                        database_handler(data_shippinginfo[0])
                return None
        if((len(data_tradinginfo)==1)and(data_current[trading_current]==1)):
                if(askyesno(title="Delete?",message="The Shipping Record cannot exist with Trading Info that has no Container Info.\nThis Shipping Record will also be removed after removing this Container Info where named "+str(data_container[workspace[1]][1]))==False):
                        return None
        elif((data_current[trading_current]==1)):
                if(askyesno(title="Delete?",message="This Container info where ID named, "+str(data_container[workspace[1]][1])+" is the only one in this Trading Info.\nThis Trading Info will be also removed for no Container Info with it.\n\nAre you sure?\n(This process is irreversible.)")==False):
                        return None
        else:
                if(askyesno(title="Delete?",message="Delete the Container info where ID named "+str(data_container[workspace[1]][1])+"?\n\nAre you sure?\n(This process is irreversible.)")==False):
                        return None
        database.execute("delete from container where container_ID=?",(data_container[workspace[1]][1],))
        if((data_current[trading_current]==1)):
                database.execute("delete from trading where trade_ID=?",(data_tradinginfo[workspace[0]][1],))
        if((len(data_tradinginfo)==1)and(data_current[trading_current]==1)):
                database.execute("delete from shipping_info where shipping_ID=?",(data_shippinginfo[0],))
                connection.commit()
                full_reset(False)
        else:
                connection.commit()
                database_handler(data_shippinginfo[0])
def del_cust():
        global windows
        menu_bar.entryconfig('Delete...',state="disabled")
        windows=Toplevel(tk)
        windows.title("Delete Customer")
        windows.attributes('-topmost',True)
        windows.focus()
        windows.resizable(False,False)
        Label(windows,text="Which customer data is going to be deleted?",font="Arial 11").grid(column=0,row=0,columnspan=2)
        Label(windows,text="Customer ID:",font="Arial 11").grid(column=0,row=1,sticky=E)
        validation=(windows.register(checkinput),'%d','%s','%S',8,3,None)
        a=(Entry(windows,font="Arial 10",validate="key",validatecommand=validation))
        a.grid(column=1,row=1)
        Button(windows,text="Confirm",command=lambda:remove_cust(a)).grid(column=1,row=3,sticky=E,ipadx=2,pady=4,padx=3)
        Button(windows,text="Close",command=close_windows).grid(column=0,row=3,sticky=W,ipadx=2,pady=4,padx=3)
        Label(windows).grid(column=2,row=0)

def remove_cust(a):
        if(a.get()==''):
                showinfo(title="Message",message="Please fill up with something.")
                return None
        if(len(a.get())!=8):
                showinfo(title="Message",message="Customer Id should be in 8 characters length.")
                return None
        database.execute("select * from customer where cust_ID=?",(a.get(),))
        if(database.fetchall()==[]):
                showinfo(title="Message",message="No record for this customer ID")
                return None
        database.execute("select * from trading where cust_ID=?",(a.get(),))
        if(database.fetchall()!=[]):
                showinfo(title="Message",message="Cannot delete this customer info for relating with some trading info.")
                return None
        if(askyesno(title="Delete?",message="Are you sure?")==False):
                return None
        database.execute("delete from customer where cust_ID=?",(a.get(),))
        connection.commit()
        showinfo(title="Message",message="Delete successfully.")

def del_item():
        global windows
        menu_bar.entryconfig('Delete...',state="disabled")
        windows=Toplevel(tk)
        windows.title("Delete Item")
        windows.attributes('-topmost',True)
        windows.focus()
        windows.resizable(False,False)
        Label(windows,text="Which item data is going to be deleted?",font="Arial 11").grid(column=0,row=0,columnspan=2)
        Label(windows,text="Item ID:",font="Arial 11").grid(column=0,row=1,sticky=E)
        validation=(windows.register(checkinput),'%d','%s','%S',6,3,None)
        a=(Entry(windows,font="Arial 10",validate="key",validatecommand=validation))
        a.grid(column=1,row=1)
        Button(windows,text="Confirm",command=lambda:remove_item(a)).grid(column=1,row=3,sticky=E,ipadx=2,pady=4,padx=3)
        Button(windows,text="Close",command=close_windows).grid(column=0,row=3,sticky=W,ipadx=2,pady=4,padx=3)
        Label(windows).grid(column=2,row=0)

def remove_item(a):
        if(a.get()==''):
                showinfo(title="Message",message="Please fill up with something.")
                return None
        if(len(a.get())!=8):
                showinfo(title="Message",message="Item Id should be in 4 characters length.")
                return None
        database.execute("select * from item where item_ID=?",(a.get(),))
        if(database.fetchall()==[]):
                showinfo(title="Message",message="No record for this item ID")
                return None
        database.execute("select * from container where item_ID=?",(a.get(),))
        if(database.fetchall()!=[]):
                showinfo(title="Message",message="Cannot delete this item info for relating with some container info.")
                return None
        if(askyesno(title="Delete?",message="Are you sure?")==False):
                return None
        database.execute("delete from item where item_ID=?",(a.get(),))
        connection.commit()
        showinfo(title="Message",message="Delete successfully.")

def close_windows():
        global windows
        menu_bar.entryconfig('Delete...',state="normal")
        windows.destroy()

def searching_choice_handler():
        global choiceB,choiceC,frame2,search_list,search_contain
        choiceC.set(0)
        if(search_contain!=[]):
                for a in range(len(search_contain)):
                        search_contain[a].grid_forget()
                search_contain=[]
        for a in range(len(search_list[choiceB.get()])):
                search_contain.append(Radiobutton(frame2,font="Arial 10",justify=LEFT,text=search_list[choiceB.get()][a],variable=choiceC,value=a))
                search_contain[a].grid(column=0,row=11+a,sticky=W)

def generate_search(a):
        global database_command,search_window,search_result        
        display_table=(('SHIPPING INFO\n\n','TRADING INFO\n\n','SHIP INFO\n\n','CUSTOMER\n\n','CONTAINER\n\n','ITEM\n\n','ADMIN\n\n'),(('Shipping ID','Ship ID','Departure Time'),('Trading ID','Admin ID','Customer ID','Shipping ID','Order Date & Time','Order Price','Destination','Priority'),('Ship ID','Ship Name','Ship Type','Total Weight Support (ton)','Control Tower Position','Internal Type','Bay for Reefer (on deck)','Bay for Reefer (internal)'),('Customer ID','First Name','Last Name','Phone no.','Email','Company','Address'),('Container ID','Trade ID','Item ID','Type','Size','Payload (kg)','Amount'),('Item ID','Item Name','Item Type','Securing Way'),('Admin ID','First Name','Last Name','Phone no.','Email','Address')),((11,7,19),(10,9,11,11,19,11,30,8),(7,15,9,26,22,13,24,25),(11,20,20,15,26,25,80),(12,8,7,8,4,12,6),(7,20,15,12),(9,20,20,15,25,80)))
        if(a==True):
                database.execute("select "+database_command[2][choiceB.get()]+" from "+database_command[0][choiceB.get()])
        else:
                a='%'+filter_word.get()+'%'
                if(filter_word.get()==''):
                        showinfo(title="Message",message="You should type something in entry\nfor filtering search.")
                        return None
                database.execute("select distinct "+database_command[2][choiceB.get()]+" from "+database_command[0][choiceB.get()]+" where "+database_command[1][choiceB.get()][choiceC.get()]+" like ? order by "+database_command[1][choiceB.get()][choiceC.get()],(a,))
        obtained=database.fetchall()
        if(len(obtained)==0):
                c='No data can be displayed.'
        else:
                c='You can tabulate the data in small font by emphasizing \'|\' as separators and\nthere are only 2 columns and 3 rows have to be removed.\n( Microsoft Office, LibreOffice, iWork or etc are having this function.)\n\n\n'+display_table[0][choiceB.get()]
                for a in range(len(display_table[1][choiceB.get()])):
                        c=c+'+-'+('').ljust(display_table[2][choiceB.get()][a]+1,'-')
                c=c+'+\n'
                for a in range(len(display_table[1][choiceB.get()])):
                        c=c+'| '+display_table[1][choiceB.get()][a].ljust(display_table[2][choiceB.get()][a]+1)
                c=c+'|\n'
                for a in range(len(display_table[1][choiceB.get()])):
                        c=c+'+-'+('').ljust(display_table[2][choiceB.get()][a]+1,'-')
                c=c+'+\n'
                for a in obtained:
                        for b in range(len(a)):
                                c=c+'| '+str(a[b]).ljust(display_table[2][choiceB.get()][b]+1)
                        c=c+'|\n'
                for a in range(len(display_table[1][choiceB.get()])):
                        c=c+'+-'+('').ljust(display_table[2][choiceB.get()][a]+1,'-')
                c=c+'+\n'
        if ((search_window is None )or (search_window.winfo_exists()==False)):
                search_window=Toplevel(tk)
                search_window.title("Search")
                search_window.attributes('-topmost',True)
                search_window.attributes('-topmost',False)
                yscroll=Scrollbar(search_window)
                yscroll.pack(side=RIGHT,fill=Y)
                xscroll=Scrollbar(search_window,orient=HORIZONTAL)
                xscroll.pack(side=BOTTOM,fill=X)
                search_result=Text(search_window,wrap=NONE,yscrollcommand=yscroll.set,xscrollcommand=xscroll.set)
                search_result.insert(END,c)
                search_result.pack(expand=True,fill=BOTH)
                yscroll.config(command=search_result.yview)
                xscroll.config(command=search_result.xview)
        else:
                search_result.delete('1.0','end')
                search_result.insert(END,c)
                search_window.attributes('-topmost',True)
                search_window.attributes('-topmost',False)
        search_window.focus()

def getUserinput(goNextPage):
        global data_list,data_entry_control,page,Input_clicked
        if(goNextPage==True):
                for a in range(data_list[2][page[0]],data_list[2][page[0]+1],1):
                        if(a>16 and a<20):
                                continue
                        if(len(Input[a].get())==0):
                                showinfo(title='Message',message="Please fill up all fleids.")
                                return False
                        if((a==0)or(a>2 and a<5)or(a==10)or(a==20)or(a==25)or(a==28)):
                                if(len(Input[a].get())!=data_entry_control[0][a]):
                                        showinfo(title='Message',message=data_list[1][page[0]][a-data_list[2][page[0]]]+" is not in "+str(data_entry_control[0][a])+" characters.")
                                        return False
                        if((a==1)or(a==5)):
                                try:
                                        b=time.strptime(Input[a].get(),'%d-%b-%y')
                                        Input[a].delete(0,END)
                                        c=int(time.strftime('%y',b))
                                        Input[a].insert(0,c)
                                        if(c<10):
                                                Input[a].insert(0,0)
                                        Input[a].insert(0,'-')
                                        c=time.strftime('%b',b)
                                        Input[a].insert(0,c)
                                        Input[a].insert(0,'-')
                                        c=int(time.strftime('%d',b))
                                        Input[a].insert(0,c)
                                        if(c<10):
                                                Input[a].insert(0,0)
                                except:
                                        showinfo(title='Message',message="Please use 'dd-mmm-yy' format in date.\nEg: 25-Aug-15")
                                        return False
                        if(a==3):
                                database.execute("select ship_ID from ship where ship_ID=?",(Input[3].get(),))
                                if(database.fetchall()==[]):
                                        showinfo(title="Message",message="You don't have this ship where ID named, "+str(Input[3].get()))
                                        return False
                                c=time.strptime(Input[1].get(),'%d-%b-%y')
                                from time import mktime
                                from datetime import datetime
                                d=datetime.fromtimestamp(mktime(c))
                                import datetime
                                c=[]
                                for i in range(13):
                                        c.append(d+datetime.timedelta(days=6-i))
                                        c[i]=str(c[i])[0:10]
                                        c[i]=time.strptime(c[i],'%Y-%m-%d')
                                        c[i]=time.strftime('%d-%b-%y',c[i])+'%'
                                        database.execute("select shipping_ID,depart_time from shipping_info where ship_ID=? and depart_time like ?",(Input[3].get(),c[i],))
                                        e=database.fetchall()
                                        if(e!=[]):
                                                if(Input[0].get()!=e[0][0]):
                                                        showinfo(title="Message",message="The ship is still in used for one week period by :\n\n[Shipping ID]\n- "+str(e[0][0]+"\n\n[Shipping Time]\n- "+str(e[0][1])))
                                                        return False
                        if((a==2)or(a==6)):
                                try:
                                        b=time.strptime(Input[a].get(),'%H:%M:%S')
                                except:
                                        showinfo(title='Message',message="Please use 'HH:MM:SS' (24-hour format) in time.\nEg: 23:50:00")
                                        return False
                        if(a==9):
                                if(Input[a].get()=='0'):
                                        showinfo(title='Message',message="Unloading priority range: 1 - 9.")
                                        return False
                        if(a==7)or(a==23):
                                try:
                                        b=float(Input[a].get())
                                        Input[a].delete(0,END)
                                        Input_clicked[a]=False
                                        Input[a].insert(0,("%.2f" % b))
                                except:
                                        showinfo(title='Message',message="Please input a proper price number.")
                                        return False
                        if(a==21):
                                if((Input[a].get()=='dry')or(Input[a].get()=='tank')or(Input[a].get()=='reefer')):
                                        continue
                                else:
                                        showinfo(title='Message',message="Container Type should be 'dry' or 'reefer' or 'tank' only.")
                                        return False
                        if(a==22):
                                if((Input[a].get()=='20')or(Input[a].get()=='40')):
                                        continue
                                else:
                                        showinfo(title='Message',message="Container Size should be '20' or '40' only.")
                                        return False
                for a in range(data_list[2][page[0]],data_list[2][page[0]+1],1):
                        Input_get[a]=Input[a].get()
        if((page[0]<4)or(goNextPage==False)):
                for a in range(data_list[2][page[0]],data_list[2][page[0]+1],1):
                        Input[a].grid_forget()
        return True

def newinput(a,b):
        global frame2,frame3
        Input_clicked.append(False)
        if(b==True):
                validation=(frame2.register(checkinput),'%d','%s','%S',20,0,None)
                Input.append(Entry(frame2,fg="grey", font="Arial 10 italic",validate="key",validatecommand=validation))
                Input[a].bind("<FocusIn>",lambda event: userinput(a,True))
                Input[a].grid(row=1+a,column=1,padx=6,sticky=W)
        else:
                validation=(frame3.register(checkinput),'%d','%s','%S',data_entry_control[0][a],data_entry_control[1][a],a)
                Input.append(Entry(frame3,bg='light grey',fg='grey',font="Arial 10 italic",validate="key",validatecommand=validation))
                Input[a].bind("<FocusIn>",lambda event: userinput(a,False))
                Input_get.append('')
    
def userinput(a,b):
        global Input_clicked,loadstate,state,exit_state
        try:
                if(Input_clicked[a]==False):
                        if(loadstate==False):
                                Input[a].delete(0,END)
                        Input[a].config(fg="black",font="Arial 10 normal")
                        Input_clicked[a]=True
                if(exit_state==False):
                        state=False
        except:
                pass

def checkinput(input_type,valid_length,last_input,a,b,num):
        global Input_clicked,state
        #1-digit,2-alpha,3=alnum,4=date,5=time,6=floating point,7=no uppercase
        if(input_type=='0'):
                return True
        elif(len(valid_length)>int(a)-1):
                return False
        if(b=='1'):
                return last_input.isdigit()
        elif(b=='2'):
                return last_input.isalpha()
        elif(b=='3'):
                return last_input.isalnum()
        elif(b=='4'):
                if ((last_input.isalnum()==True) or last_input=='-'):
                        return True
                if(Input_clicked[int(num)]==True):
                        return False
                else:
                        return True
        elif(b=='5'):
                if ((last_input.isdigit()==True) or last_input==':'):
                        return True
                if(Input_clicked[int(num)]==True):
                        return False
                else:
                        return True
        elif(b=='6'):
                if ((last_input.isdigit()==True) or last_input=='.'):
                        return True
                if(Input_clicked[int(num)]==True):
                        return False
                else:
                        return True
        elif(b=='7'):
                return (last_input.lower()==last_input)
        else:
                return True
                        
def version():
        global window
        window=Toplevel(tk)
        window.title("About Software")
        window.attributes('-topmost',True)
        window.attributes('-topmost',False)
        window.focus()
        Label(window,text="Software Name: Container Loading System\nVersion 0.1\n\n@Workshop 1\n@Degree in Computer Science\n@Universiti Teknikal Malaysia Melaka\n@in 2015").pack(ipadx=20,ipady=4)
        about_button.entryconfig('About Software',state="disabled")
        window.wm_protocol('WM_DELETE_WINDOW',exit_version)
        
def exit_version():
        global window
        about_button.entryconfig('About Software',state="normal")
        window.destroy()

def arrange_system():   ################################## algorithm functions #######################################
    global Input_get
    database.execute("select weight_support,ship_dimension,internal_type,reefer_place_top,reefer_place_bottom,control_tower from ship where ship_ID=?",(Input_get[3],))
    temp=database.fetchall() #ship info
    ship=[]
    ship.append(list(temp[0]))
    temp=ship[0][3].split('-')
    ship[0][3]=int(temp[0])
    ship[0].insert(4,int(temp[1]))
    temp=ship[0][5].split('-')
    ship[0][5]=int(temp[0])
    ship[0].insert(6,int(temp[1]))

    database.execute("select trade_ID from trading where shipping_ID=?",(Input_get[0],))
    trade=database.fetchall()   #trade ID that matches with specific ship

    payload=0   #calculate payload
    for i in range(len(trade)):
        database.execute("select sum(payload) from container where trade_ID=?",(trade[i][0],))
        temp=database.fetchall()
        payload=payload+temp[0][0]
    if((payload/1000)>ship[0][0]):
        showinfo(title='Message',message="The containers are too heavy for this ship.\nContainer weight : "+str(payload[0]/1000)+'ton\nShip support : '+str(weight_support[0]))
        return None

    container_info,temp=[],[]  #container_info
    for i in range(len(trade)):
        database.execute("select trade_ID,amount,container_size,container_type,payload,container_ID from container where trade_ID=?",(trade[i][0],))
        temp.append(database.fetchall())
    for i in range(len(temp)):
        for j in range(len(temp[i])):
            container_info.append(list(temp[i][j]))
    for i in range(len(container_info)):
        container_info[i][2]=int(container_info[i][2])

    temp2=0     #taken space by container
    for i in range(len(container_info)):
        temp2=temp2+container_info[i][1]*(container_info[i][2]/20)

    dim=ship[0][1].split(',')   #ship structure = view
    for i in range(len(dim)):
        dim[i]=int(dim[i])
    placing,temp7,view,temp3=[],[],[],[]
    for i in range(dim[0]):
        temp3.append(dim[2])
        temp7.append([])
    for i in range(16):
        view.append(deepcopy(temp3))
        placing.append(deepcopy(temp7))

    if(ship[0][7]=='sturn'):#sturn
        temp3=[[0],[1,2],[3,4,15]]
    else:
        temp3=[[0,15],[1,14],[2,13]]
    if(ship[0][2]=='wide'):#wide
        temp=2
    else:
        temp=0
    condition=True
    for h in range(len(temp3)):
        for j in range(len(temp3[h])):
            if(dim[2]%2==0):
                for i in range(dim[0]-dim[1]):
                    if(dim[0]-dim[1]-i<3):
                        break
                    else:
                        view[temp3[h][j]][len(view[temp3[h][j]])-1-i]=2*(i+1)+temp+h*2
                        if(view[temp3[h][j]][len(view[temp3[h][j]])-1-i]==dim[2]):
                            break
            else:
                for i in range(dim[0]-dim[1]):
                    if(dim[0]-dim[1]-i<3):
                        break
                    else:
                        view[temp3[h][j]][len(view[temp3[h][j]])-1-i]=2*(i)+1+temp+h*2
                        if(view[temp3[h][j]][len(view[temp3[h][j]])-1-i]==dim[2]):
                            break
            if(view[temp3[h][j]][len(view[temp3[h][j]])-1]==dim[2]):
                condition=False
                break
        if(condition==False):
            break;

    ship_space=0    #ship space
    for i in range(len(view)):
        for j in range(len(view[i])):
            ship_space=ship_space+view[i][j]
    if(temp2>ship_space):
        showinfo(title='Message',message="There is no enough space for all container to be loaded on ship.")
        return None

    temp,temp2=0,0     #reefer space
    if(ship[0][3]!=0):
        for i in range(ship[0][3],ship[0][4]+1,1):
            if(dim[1]-dim[2]>1):
                temp2=temp2+view[i-1][len(view[i-1])-1]+view[i-1][len(view[i-1])-2]
            else:
                temp2=temp2+view[i-1][len(view[i-1])-1]
    if(ship[0][5]!=0):
        for i in range(ship[0][5],ship[0][6]+1,1):
            temp2=temp2+dim[2]*2

    for i in range(len(container_info)):    #number of reefer
        if(container_info[i][3]=='reefer'):
            temp=temp+container_info[i][1]*(container_info[i][2]/20)
    if(temp>temp2):
        showinfo(title='Message',message="There is no enough space for container (reefer type).")
        return None
    reefer_no=deepcopy(temp)

    tank,temp4=0,0  #number of tank
    for i in range(len(container_info)):
        if(container_info[i][3]=='tank'):
            tank=tank+container_info[i][1]*(container_info[i][2]/20)

    for j in range(16):  #internal space
        for i in range(dim[1]):
            temp4=temp4+view[j][len(view[j])-1-i]
    if(tank>temp4):
        showinfo(title='Message',message="There is no enough internal* space for all containers (tank type)\nto be pumped into LNG collector tank.")
        return None

    temp,temp2,temp3,temp4,temp5,temp6=0,15,0,0,0,0
    if(ship[0][5]!=0):  #check are reefer and tank competing for place
        while(view[temp][len(view[temp])-1]!=dim[2]):    #look for suitable tank space,temp and temp2
            temp=temp+1
        while(view[temp2][len(view[temp])-1]!=dim[2]):
            temp2=temp2-1
        ispace=(temp2-temp+1)*dim[2]*dim[1]
        if(temp<=ship[0][5]-1): #reefer overlaps wide internal space (tank use),temp3 and temp4
            temp3=ship[0][5]-1
        else:
            temp3=deepcopy(temp)
        if(temp2>=ship[0][6]-1):
            temp4=ship[0][6]-1
        else:
            temp4=deepcopy(temp2)
        if((temp3>=temp2)and(temp4<=temp)):
            for i in range(temp,temp2+1,1): #total internal space
                for j in range(dim[1]):
                    temp5=temp5+view[i][len(view[j])-1-j]
        
            for i in range(temp3,temp4+1,1): #total reefer place at internal space
                for j in range(dim[1]):
                    temp6=temp6+view[i][len(view[j])-1-j]
            if(tank>temp5-temp6):
                showinfo(title='Message',message="Reefer and tank type containers are competing for internal spaces.\nThe LNG collector tank cannot mix with others at same internal bays.\n\n(Removing either one can free up the space for another.)")
                return None

    dry,reefer=[],[]
    for i in range(len(container_info)):
        database.execute("select priority from trading where trade_ID=?",(container_info[i][0],))
        z=database.fetchall()
        container_info[i].insert(5,int(z[0][0]))
        if(container_info[i][3]=='dry'):    #dry container
            dry.append(container_info[i])
        elif(container_info[i][3]=='reefer'):   #reefer
            reefer.append(container_info[i])
    dry=sorted(dry,key=lambda x:(x[2],-x[4],-x[5]))
    reefer=sorted(reefer,key=lambda x:(x[2],-x[4],-x[5]))

    temp_reefer,whose,h,g=[],1,len(reefer),len(reefer)
    if(len(reefer)!=0):
        if(ship[0][5]!=0):  #dealing with reefer place (below the deck)
            for k in range(1,3,1):
                for i in range(ship[0][5]-1,ship[0][6],1):
                    for j in range(view[i][dim[0]-k]):
                        if(len(placing[i][dim[0]-k])<view[i][dim[0]-k]):
                            if(reefer[h-g][1]!=0):
                                if(reefer[h-g][2]==20):
                                    placing[i][dim[0]-k].append(whose)
                                    reefer[h-g][1]=reefer[h-g][1]-1
                                else:
                                    while(len(placing[i][dim[0]-k])!=j+1):
                                        if(reefer[h-g][1]==0):
                                            if(g!=1):
                                                whose=whose+1
                                                g=g-1
                                            else:
                                                break
                                        if(i+1<ship[0][6]):
                                            if((len(placing[i][dim[0]-k])%2==1)and(len(placing[i][dim[0]-k])!=len(placing[i+1][dim[0]-k]))):
                                                temp_reefer.append(placing[i][dim[0]-k][0])
                                                del placing[i][dim[0]-k][0]
                                            while(len(placing[i][dim[0]-k])!=len(placing[i+1][dim[0]-k])):
                                                placing[i+1][dim[0]-k].insert(0,placing[i][dim[0]-k][0])
                                                del placing[i][dim[0]-k][0]
                                            placing[i][dim[0]-k].append(str(whose)+'a')
                                            placing[i+1][dim[0]-k].append(str(whose)+'b')
                                            reefer[h-g][1]=reefer[h-g][1]-1
                                        else:
                                            break
                            else:
                                if(g!=1):
                                    whose=whose+1
                                    g=g-1
                                    if(reefer[h-g][2]==20):
                                        placing[i][dim[0]-k].append(whose)
                                        reefer[h-g][1]=reefer[h-g][1]-1
                                    else:
                                        while(len(placing[i][dim[0]-k])!=j+1):
                                            if(reefer[h-g][1]==0):
                                                if(g!=1):
                                                    whose=whose+1
                                                    g=g-1
                                                else:
                                                    break
                                            if(i+1<ship[0][6]):
                                                if((len(placing[i][dim[0]-k])%2==1)and(len(placing[i][dim[0]-k])!=len(placing[i+1][dim[0]-k]))):
                                                    temp_reefer.append(placing[i][dim[0]-k][0])
                                                    del placing[i][dim[0]-k][0]
                                                while(len(placing[i][dim[0]-k])!=len(placing[i+1][dim[0]-k])):
                                                    placing[i+1][dim[0]-k].insert(0,placing[i][dim[0]-k][0])
                                                    del placing[i][dim[0]-k][0]
                                                placing[i][dim[0]-k].append(str(whose)+'a')
                                                placing[i+1][dim[0]-k].append(str(whose)+'b')
                                                reefer[h-g][1]=reefer[h-g][1]-1
                                            else:
                                                break

        if(ship[0][3]!=0):  #dealing with reefer place (on deck)
            for k in range(1,3,1):
                for i in range(ship[0][3]-1,ship[0][4],1):
                    for j in range(view[i][dim[0]-dim[1]-k]):
                        if(len(placing[i][dim[0]-dim[1]-k])<view[i][dim[0]-dim[1]-k]):
                            if(reefer[h-g][1]!=0):
                                if(reefer[h-g][2]==20):
                                    placing[i][dim[0]-dim[1]-k].append(whose)
                                    reefer[h-g][1]=reefer[h-g][1]-1
                                else:
                                    while(len(placing[i][dim[0]-dim[1]-k])!=j+1):
                                        if(reefer[h-g][1]==0):
                                            if(g!=1):
                                                whose=whose+1
                                                g=g-1
                                            else:
                                                break
                                        if(i+1<ship[0][4]):
                                            if((len(placing[i][dim[0]-dim[1]-k])%2==1)and(len(placing[i][dim[0]-dim[1]-k])!=len(placing[i+1][dim[0]-dim[1]-k]))):
                                                temp_reefer.append(placing[i][dim[0]-dim[1]-k][0])
                                                del placing[i][dim[0]-dim[1]-k][0]
                                            while(len(placing[i][dim[0]-dim[1]-k])!=len(placing[i+1][dim[0]-dim[1]-k])):
                                                placing[i+1][dim[0]-dim[1]-k].insert(0,placing[i][dim[0]-dim[1]-k][0])
                                                del placing[i][dim[0]-dim[1]-k][0]
                                            placing[i][dim[0]-dim[1]-k].append(str(whose)+'a')
                                            placing[i+1][dim[0]-dim[1]-k].append(str(whose)+'b')
                                            reefer[h-g][1]=reefer[h-g][1]-1
                                        else:
                                            break
                            else:
                                if(g!=1):
                                    whose=whose+1
                                    g=g-1
                                    if(reefer[h-g][2]==20):
                                        placing[i][dim[0]-dim[1]-k].append(whose)
                                        reefer[h-g][1]=reefer[h-g][1]-1
                                    else:
                                        while(len(placing[i][dim[0]-dim[1]-k])!=j+1):
                                            if(reefer[h-g][1]==0):
                                                if(g!=1):
                                                    whose=whose+1
                                                    g=g-1
                                                else:
                                                    break
                                            if(i+1<ship[0][4]):
                                                if((len(placing[i][dim[0]-dim[1]-k])%2==1)and(len(placing[i][dim[0]-dim[1]-k])!=len(placing[i+1][dim[0]-dim[1]-k]))):
                                                    temp_reefer.append(placing[i][dim[0]-dim[1]-k][0])
                                                    del placing[i][dim[0]-dim[1]-k][0]
                                                while(len(placing[i][dim[0]-dim[1]-k])!=len(placing[i+1][dim[0]-dim[1]-k])):
                                                    placing[i+1][dim[0]-dim[1]-k].insert(0,placing[i][dim[0]-dim[1]-k][0])
                                                    del placing[i][dim[0]-dim[1]-k][0]
                                                placing[i][dim[0]-dim[1]-k].append(str(whose)+'a')
                                                placing[i+1][dim[0]-dim[1]-k].append(str(whose)+'b')
                                                reefer[h-g][1]=reefer[h-g][1]-1
                                            else:
                                                break

        if(temp_reefer!=[]):
            if(ship[0][5]!=0):
                for k in range(1,3,1):
                    for i in range(ship[0][5]-1,ship[0][6],1):
                        for j in range(view[i][dim[0]-k]):
                            if((len(placing[i][dim[0]-k])<view[i][dim[0]-k])and(temp_reefer!=[])):
                                placing[i][dim[0]-k].append(temp_reefer[0])
                                del temp_reefer[0]
                                break
                            break
        if(temp_reefer!=[]):
            if(ship[0][3]!=0):
                for k in range(1,3,1):
                    for i in range(ship[0][3]-1,ship[0][4],1):
                        for j in range(view[i][dim[0]-dim[1]-k]):
                            if((len(placing[i][dim[0]-dim[1]-k])<view[i][dim[0]-dim[1]-k])and(temp_reefer!=[])):
                                 placing[i][dim[0]-dim[1]-k].append(temp_reefer[0])
                                 del temp_reefer[0]
                                 break
                            break
    tank_cond=True
    if(tank!=0):
        temp10,temp9,temp8,temp5,temp6=0,-1,0,ceil(tank/dim[1]/dim[2]),tank/dim[1]/dim[2]
        if((ship[0][5]!=0)and(len(reefer)!=0)):
            if(temp5!=temp6):   #LNG collector tank, if not fully filled, tank_cond = False
                tank_cond=False
            while(len(placing[temp4])==0):
                temp4=temp4+1
            if(temp3-temp>temp2-temp4): #choosing larger place for tank. If both equal choose larger bay no.
               temp7,temp8=deepcopy(temp3),deepcopy(temp)
            else:
                temp7,temp8,temp9=deepcopy(temp4),deepcopy(temp2),1
            for j in range(temp5):  #arrange tank
                i=temp7+temp9*(j+1)
                if((i<temp)or(i>temp2)):
                    break;
                for k in range(dim[1]):
                    for l in range(view[i][dim[0]-k-1]):
                        placing[i][dim[0]-k-1].append('x')
                temp10=temp10+1
            if(temp10!=temp5):  #start placing at another place, if reefer at center.
                if(temp3-temp>temp2-temp4):
                    temp7,temp8,temp9=deepcopy(temp4),deepcopy(temp2),1
                else:
                    temp7,temp8,temp9=deepcopy(temp3),deepcopy(temp),-1
                for j in range(temp5-temp10):   # arrange tank
                    i=temp7+temp9*(j+1)
                    if((i<temp)or(i>temp2)):
                        break;
                    for k in range(dim[1]):
                        for l in range(view[i][dim[0]-k-1]):
                            placing[i][dim[0]-k-1].append('x')
        else:
            if(ship[0][7]=='sturn'):    #place hehind
                for i in range(temp5):
                    for k in range(dim[1]):
                        for l in range(view[temp2-i][dim[0]-k-1]):
                            placing[temp2-i][dim[0]-k-1].append('x')
            else:
                place_centerx,temp,temp2=0,7,8  #center, put at larger place
                while(tank>place_centerx):
                    for i in range(temp,temp2+1,1):
                        for j in range(len(view[i])):
                            place_centerx=place_centerx+view[i][j]
                    if(tank>place_centerx):
                        if((temp2-temp)%2==1):
                            temp=temp-1
                        else:
                            temp2=temp2+1
                for i in range(temp5):
                    for k in range(dim[1]):
                        for l in range(view[temp2-i][dim[0]-k-1]):
                            placing[temp2-i][dim[0]-k-1].append('x')
                            
    a,b,c,d=0,0,0,0     #front and behind empty gaps, if both odd and condition allows, all arranged containers will move backward.
    while(len(placing[a][dim[0]-1])!=0):
        if(a!=15):
            a=a+1
        else:
            break
    if(a!=15):
        b=deepcopy(a)
        while(len(placing[b][dim[0]-1])==0):
            if(b!=15):
                b=b+1
            else:
                break
        if(b==15):
            b=16
        if(b!=16):
            c=deepcopy(b)
            while(len(placing[c][dim[0]-1])!=0):
                if(c!=15):
                    c=c+1
                else:
                    break
            if(c!=15):
                d=deepcopy(c)
                while(len(placing[d][dim[0]-1])==0):
                    if(d!=15):
                        d=d+1
                    else:
                        break
                if(d==15):
                    d=16
                if(((b-a)%2==1)and((d-c)%2==1)and(view[c][dim[0]-1]==dim[2])and((((ship[0][6]-ship[0][5]+1)*dim[2])-dim[2])>=reefer_no)):
                    placing.insert(b-1,placing[c])
                    del placing[c+1]

    if(len(dry)!=0):                    
        count=0     #filling dry regardless 20' or 40'
        for i in range(len(dry)):
            count=count+(dry[i][1]*(dry[i][2]/20))
        for i in range(dim[0]):
            for j in range(16):
                while(view[j][dim[0]-1-i]>len(placing[j][dim[0]-1-i])):
                    if(count!=0):
                        placing[j][dim[0]-1-i].append('?')
                        count=count-1
                    else:
                        break

        temp_cont,condition,condition2,m,n=[],True,True,0,0     #create more space for 40
        for i in range(dim[0]):
            for j in range(16):
                if(len(placing[j][i])!=0):
                    m,n=deepcopy(j),deepcopy(j)
                    while(len(placing[n][i])!=0):
                        if(n!=15):
                            n=n+1
                        else:
                            break
                    if(n==15):
                        n=16
                    for k in range(len(placing[n-1][i])):
                        if(placing[n-1][i][k]!='?'):
                            condition=False
                    for k in range(len(placing[n-2][i])):
                        if(placing[n-2][i][k]!='?'):
                            condition2=False
                    if(n==0):
                        n=n+1
                    if((condition==True)and(condition2==True)):
                        if((n-m)%2==1):
                            if(len(placing[n-1][i])%2!=0):
                                temp_cont.append('?')
                                placing[n-1][i].pop()
                            while(len(placing[n-1][i])!=len(placing[n-2][i])):
                                placing[n-1][i].append('?')
                                placing[n-2][i].pop()
                        else:
                            if((len(placing[n-1][i])+len(placing[n-2][i]))%2!=0):
                                temp_cont.append('?')
                                placing[n-1][i].pop()
                            while(len(placing[n-1][i])!=len(placing[n-2][i])):
                                placing[n-1][i].append('?')
                                placing[n-2][i].pop()
                        condition,condition2=False,False
                    break
                break

        whose2,h=len(reefer)+len(dry),len(dry)-1    #dealing with 40' only
        for i in range(dim[0]):
            for j in range(15):
                if(len(placing[j][i])!=0):
                    condition,condition2=True,True
                    for k in range(len(placing[j][i])):
                        if(placing[j][i][k]!='?'):
                            condition=False
                    for k in range(len(placing[j+1][i])):
                        if(placing[j+1][i][k]!='?'):
                            condition2=False
                    try:
                        if(i+1!=dim[0]):
                            if((placing[j][i+1][0]!='?')and(placing[j+1][i+1][0]=='?')):
                                    condition2=False
                            if((placing[j][i+1][0]=='x')or(placing[j+1][i+1][0]=='x')):
                               condition2=True
                    except:
                        condition2=False
                    if((condition==True)and(condition2==True)):
                        step=0
                        if(len(placing[j+1][i])==len(placing[j][i])):
                            while(len(placing[j][i])>step):
                                if(dry[h][1]!=0):
                                    if(dry[h][2]==40):
                                        placing[j][i][step]=str(whose2)+'a'
                                        placing[j+1][i][step]=str(whose2)+'b'
                                        dry[h][1]=dry[h][1]-1
                                else:
                                    if(h!=0):
                                        whose2=whose2-1
                                        h=h-1
                                        if(dry[h][2]==40):
                                            placing[j][i][step]=str(whose2)+'a'
                                            placing[j+1][i][step]=str(whose2)+'b'
                                            dry[h][1]=dry[h][1]-1
                                step=step+1
                        elif(len(placing[j+1][i])>len(placing[j][i])):
                            while(len(placing[j][i])>step):
                                if(dry[h][1]!=0):
                                    if(dry[h][2]==40):
                                        placing[j][i][step]=str(whose2)+'a'
                                        placing[j+1][i][step+int((len(placing[j+1][i])-len(placing[j][i]))/2)]=str(whose2)+'b'
                                        dry[h][1]=dry[h][1]-1
                                else:
                                    if(h!=0):
                                        whose2=whose2-1
                                        h=h-1
                                        if(dry[h][2]==40):
                                            placing[j][i][step]=str(whose2)+'a'
                                            placing[j+1][i][step+int((len(placing[j+1][i])-len(placing[j][i]))/2)]=str(whose2)+'b'
                                            dry[h][1]=dry[h][1]-1
                                step=step+1
                        else:
                            while(len(placing[j+1][i])>step):
                                if(dry[h][1]!=0):
                                    if(dry[h][2]==40):
                                        placing[j][i][step+int((len(placing[j][i])-len(placing[j+1][i]))/2)]=str(whose2)+'a'
                                        placing[j+1][i][step]=str(whose2)+'b'
                                        dry[h][1]=dry[h][1]-1
                                else:
                                    if(h!=0):
                                        whose2=whose2-1
                                        h=h-1
                                        if(dry[h][2]==40):
                                            placing[j][i][step+int((len(placing[j][i])-len(placing[j+1][i]))/2)]=str(whose2)+'a'
                                            placing[j+1][i][step]=str(whose2)+'b'
                                            dry[h][1]=dry[h][1]-1
                                step=step+1
                                
        if(len(reefer)!=0):     #fill 20'
            whose=whose+1
        g,h=len(dry),len(dry)
        for i in range(dim[0]-1,-1,-1):
            for j in range(16):
                step=0
                if(len(placing[j][i])!=0):
                    while(len(placing[j][i])>step):
                        if(placing[j][i][step]=='?'):
                            try:
                                    if(dry[h-g][1]!=0):
                                        if(dry[h-g][2]==20):
                                            placing[j][i][step]=whose
                                            dry[h-g][1]=dry[h-g][1]-1
                                    else:
                                        if(g!=0):
                                            whose=whose+1
                                            g=g-1
                                            if(dry[h-g][2]==20):
                                                placing[j][i][step]=whose
                                                dry[h-g][1]=dry[h-g][1]-1
                            except:
                                    pass
                        step=step+1
                        
        for i in range(dim[0]):     #put stacker to fully use space or prevent shaking
            for j in range(16):
                step=0
                if(len(placing[j][i])!=0):
                    while(len(placing[j][i])>step):
                        if(placing[j][i][step]=='?'):
                            placing[j][i][step]='+'
                        step=step+1
        cont,cond=0,True
        for i in range(dim[0]):
            if(cond==True):
                cont=deepcopy(i)
                for j in range(16):
                    if(len(placing[j][i])!=0):
                        cond=False
                        step=0
                        while(len(placing[j][i])>step):
                            if(placing[j][i][step]=='+'):
                                del placing[j][i][step]
                                step=step-1
                            step=step+1
        
        h,cond=0,True
        for i in range(len(dry)):
            if((cond==True)and(dry[len(dry)-1-i][1]!=0)and(dry[len(dry)-1-i][2]==40)):
                h,cond=len(dry)-1-i,False
        for i in range(cont,-1,-1):
            for j in range(15):
                if(len(placing[j][i])<view[j][i]):
                    condition,condition2=True,True
                    if(len(placing[j][i])==view[j][i]):
                        condition=False
                    if(len(placing[j+1][i])==view[j+1][i]):
                        condition2=False
                    if((condition==True)and(condition2==True)):
                        step=0
                        if(view[j+1][i]==view[j][i]):
                            while(view[j][i]>step):
                                if(dry[h][1]!=0):
                                    if(dry[h][2]==40):
                                        placing[j][i].append(str(whose2)+'a')
                                        placing[j+1][i].append(str(whose2)+'b')
                                        dry[h][1]=dry[h][1]-1
                                else:
                                    if(h!=0):
                                        whose2=whose2-1
                                        h=h-1
                                        if(dry[h][2]==40):
                                            placing[j][i].append(str(whose2)+'a')
                                            placing[j+1][i].append(str(whose2)+'b')
                                            dry[h][1]=dry[h][1]-1
                                step=step+1
                        elif(view[j+1][i]>view[j][i]):
                            while(view[j][i]>step):
                                if(dry[h][1]!=0):
                                    if(dry[h][2]==40):
                                        placing[j][i].append(str(whose2)+'a')
                                        placing[j+1][i].append(str(whose2)+'b')
                                        dry[h][1]=dry[h][1]-1
                                else:
                                    if(h!=0):
                                        whose2=whose2-1
                                        h=h-1
                                        if(dry[h][2]==40):
                                            placing[j][i].append(str(whose2)+'a')
                                            placing[j+1][i].append(str(whose2)+'b')
                                            dry[h][1]=dry[h][1]-1
                                step=step+1
                        else:
                            while(view[j+1][i]>step):
                                if(dry[h][1]!=0):
                                    if(dry[h][2]==40):
                                        placing[j][i].append(str(whose2)+'a')
                                        placing[j+1][i].append(str(whose2)+'b')
                                        dry[h][1]=dry[h][1]-1
                                else:
                                    if(h!=0):
                                        whose2=whose2-1
                                        h=h-1
                                        if(dry[h][2]==40):
                                            placing[j][i].append(str(whose2)+'a')
                                            placing[j+1][i].append(str(whose2)+'b')
                                            dry[h][1]=dry[h][1]-1
                                step=step+1
           
    if(tank_cond==False):
        showinfo(title='Message',message="The bay separators are needed to isolate the LNG collector tank.\nHowever, the LNG collector tank is not fully filled up.\n\n# You can add more tank container(s), about "+str(int((temp7*dim[2]*dim[1])-tank))+" unit(s).")
    root=Toplevel(tk)
    canvas=Canvas(root,width=1024,height=620)
    root.resizable(False,False)
    frame_win=Frame(canvas)
    vsb=Scrollbar(root,orient="vertical", command=canvas.yview)
    hsb=Scrollbar(root,orient="horizontal", command=canvas.xview)
    canvas.configure(yscrollcommand=vsb.set,xscrollcommand=hsb.set)
    vsb.pack(side="right", fill="y")
    hsb.pack(side="bottom", fill="x")
    canvas.pack(fill="both", expand=True)
    canvas.create_window((4,4), window=frame_win, anchor="nw")
    frame_win.bind("<Configure>", lambda event, canvas=canvas: control_canvas_size(canvas))
    newframe,countx,county=[],0,0
    for i in range(len(view)):
        newframe.append(Frame(frame_win))
        newframe[i].grid(column=countx+1,row=county,padx=5,pady=5)
        countx=countx+1
        if(countx==8):
            county=county+1
            countx=0
        for k in range(len(view[i])-dim[1]):
            for j in range(view[i][k]):
                if((dim[2]>view[i][k])):
                    try:
                        ex=Label(newframe[i],relief=GROOVE,fg='white')
                        ex.grid(row=k,column=j+int((dim[2]-view[i][k])/2))
                        if(type(placing[i][k][j])==str):
                            if(placing[i][k][j]=='+'):
                                ex.config(text=placing[i][k+dim[1]-1][j],fg='black',bg='yellow')
                            elif(int(placing[i][k][j][:1])>len(reefer)):
                                if(placing[i][k][j][len(placing[i][k][j])-1]=='a'):
                                    ex.config(text=str(int(placing[i][k][j][:1])),fg='black',bg='light green')
                                else:
                                    ex.config(text=str(int(placing[i][k][j][:1])),bg='dark green')
                            else:
                                if(placing[i][k][j][len(placing[i][k][j])-1]=='a'):
                                    ex.config(text=str(int(placing[i][k][j][:1])),fg='black',bg='light blue')
                                else:
                                    ex.config(text=str(int(placing[i][k][j][:1])),bg='dark blue')
                        else:
                            if(int(placing[i][k][j])>len(reefer)):
                                ex.config(text=placing[i][k][j],bg='red')
                            else:
                                ex.config(text=placing[i][k][j],bg='purple')
                    except:
                        Label(newframe[i],relief=GROOVE,text=' ',bg='white').grid(row=k,column=j+int((dim[2]-view[i][k])/2))
                else:
                    try:
                        ex=Label(newframe[i],relief=GROOVE,fg='white')
                        ex.grid(row=k,column=j)
                        if(type(placing[i][k][j])==str):
                            if(placing[i][k][j]=='+'):
                                ex.config(text=placing[i][k+dim[1]-1][j],fg='black',bg='yellow')
                            elif(int(placing[i][k][j][:1])>len(reefer)):
                                if(placing[i][k][j][len(placing[i][k][j])-1]=='a'):
                                    ex.config(text=str(int(placing[i][k][j][:1])),fg='black',bg='light green')
                                else:
                                    ex.config(text=str(int(placing[i][k][j][:1])),bg='dark green')
                            else:
                                if(placing[i][k][j][len(placing[i][k][j])-1]=='a'):
                                    ex.config(text=str(int(placing[i][k][j][:1])),fg='black',bg='light blue')
                                else:
                                    ex.config(text=str(int(placing[i][k][j][:1])),bg='dark blue')
                        else:
                            if(int(placing[i][k][j])>len(reefer)):
                                ex.config(text=placing[i][k][j],bg='red')
                            else:
                                ex.config(text=placing[i][k][j],bg='purple')
                    except:
                        Label(newframe[i],relief=GROOVE,text=' ',bg='white').grid(row=k,column=j+int((dim[2]-view[i][k])/2))
            Label(newframe[i],text=dim[0]-dim[1]-k).grid(row=k,column=dim[2],sticky=W)
        Label(newframe[i],bg='black').grid(row=dim[1],column=0,ipadx=dim[2]*8,columnspan=dim[2]+1)
        Label(newframe[i],text='Deck',fg='white',bg='black').grid(row=dim[1],column=0,columnspan=dim[2]+1)
        for k in range(dim[1]):
            try:
                for j in range(view[i][k+dim[1]-1]):
                        if((dim[2]>view[i][k+dim[1]-1])):
                            try:
                                ex=Label(newframe[i],relief=GROOVE,fg='white')
                                ex.grid(row=k+dim[1]+1,column=j+int((dim[2]-view[i][k+dim[1]-1])/2))
                                if(type(placing[i][k+dim[1]-1][j])==str):
                                    if(placing[i][k+dim[1]-1][j]=='x'):
                                        ex.config(text=placing[i][k+dim[1]-1][j],bg='dark grey')
                                    elif(placing[i][k+dim[1]-1][j]=='+'):
                                        ex.config(text=placing[i][k+dim[1]-1][j],fg='black',bg='yellow')
                                    elif(int(placing[i][k+dim[1]-1][j][:1])>len(reefer)):
                                        if(placing[i][k+dim[1]-1][j][len(placing[i][k+dim[1]-1][j])-1]=='a'):
                                            ex.config(text=str(int(placing[i][k+dim[1]-1][j][:1])),fg='black',bg='light green')
                                        else:
                                            ex.config(text=str(int(placing[i][k+dim[1]-1][j][:1])),bg='dark green')
                                    elif(placing[i][k+dim[1]-1][j]=='+'):
                                        ex.config(text=placing[i][k+dim[1]-1][j],bg='light grey')
                                    else:
                                        if(placing[i][k+dim[1]-1][j][len(placing[i][k+dim[1]-1][j])-1]=='a'):
                                            ex.config(text=str(int(placing[i][k+dim[1]-1][j][:1])),fg='black',bg='light blue')
                                        else:
                                            ex.config(text=str(int(placing[i][k+dim[1]-1][j][:1])),bg='dark blue')
                                else:
                                    if(placing[i][k+dim[1]-1][j]>len(reefer)):
                                        ex.config(text=placing[i][k+dim[1]-1][j],bg='red')
                                    else:
                                        ex.config(text=placing[i][k+dim[1]-1][j],bg='purple')
                            except:
                                Label(newframe[i],relief=GROOVE,text=' ',bg='white').grid(row=k+dim[1]+1,column=j+int((dim[2]-view[i][k+dim[1]-1])/2))
                        else:
                            try:
                                ex=Label(newframe[i],relief=GROOVE,fg='white')
                                ex.grid(row=k+dim[1]+1,column=j)
                                if(type(placing[i][k+dim[1]-1][j])==str):
                                    if(placing[i][k+dim[1]-1][j]=='x'):
                                        ex.config(text=placing[i][k+dim[1]-1][j],bg='dark grey')
                                    elif(placing[i][k+dim[1]-1][j]=='+'):
                                        ex.config(text=placing[i][k+dim[1]-1][j],fg='black',bg='yellow')
                                    elif(int(placing[i][k+dim[1]-1][j][:1])>len(reefer)):
                                        if(placing[i][k+dim[1]-1][j][len(placing[i][k+dim[1]-1][j])-1]=='a'):
                                            ex.config(text=str(int(placing[i][k+dim[1]-1][j][:1])),fg='black',bg='light green')
                                        else:
                                            ex.config(text=str(int(placing[i][k+dim[1]-1][j][:1])),bg='dark green')
                                    else:
                                        if(placing[i][k+dim[1]-1][j][len(placing[i][k+dim[1]-1][j])-1]=='a'):
                                            ex.config(text=str(int(placing[i][k+dim[1]-1][j][:1])),fg='black',bg='light blue')
                                        else:
                                            ex.config(text=str(int(placing[i][k+dim[1]-1][j][:1])),bg='dark blue')
                                else:
                                    if(placing[i][k+dim[1]-1][j]>len(reefer)):
                                        ex.config(text=placing[i][k+dim[1]-1][j],bg='red')
                                    else:
                                        ex.config(text=placing[i][k+dim[1]-1][j],bg='purple')
                            except:
                                Label(newframe[i],relief=GROOVE,text=' ',bg='white').grid(row=k+dim[1]+1,column=j)
            except:
                        pass
            Label(newframe[i],text=-k-1).grid(row=k+dim[1]+1,column=dim[2],sticky=W)
        Label(newframe[i],text='< BAY '+str(i+1)+' >\n\n').grid(row=dim[0]+3,column=0,columnspan=dim[2],pady=4)
    side_bar=Frame(frame_win)
    side_bar.grid(column=0,row=0,sticky=NW)
    side_bar1=Frame(side_bar,relief=SUNKEN,borderwidth=1)
    side_bar1.grid(column=0,row=0,sticky=NE,padx=4,pady=4,ipadx=2,ipady=2)
    Label(side_bar1).grid(column=0,row=0)
    Label(side_bar1,text='<<< DESCRIPTION >>>').grid(column=0,row=0,padx=2,pady=2,columnspan=3)
    colors=(('white','yellow','red','light green','dark green','purple','light blue','dark blue','dark grey'),('Empty','Retractable Medium',"20' Dry Container","40' Dry Container (front)","40' Dry Container (back)","20' Reefer","40' Reefer (front)","40' Reefer (back)",'LNG Gas Collector'))
    for i in range(len(colors[0])):
        Label(side_bar1,text='\t\t',bg=colors[0][i],relief=GROOVE,borderwidth=0.5).grid(column=1,row=1+i,padx=2,pady=2,sticky=W)
        Label(side_bar1,text=colors[1][i]).grid(column=2,row=1+i,padx=2,pady=2,sticky=W)
    side_bar3=Frame(side_bar,relief=SOLID,borderwidth=1)
    side_bar3.grid(column=0,row=1,sticky=NE,padx=4,pady=4,ipadx=2,ipady=2)
    Label(side_bar3,text="'+' = Retractable Medium").grid(column=0,row=0,sticky=NW)
    Label(side_bar3,text=" 'x' = LNG Gas Collector").grid(column=0,row=1,sticky=NW)
    side_bar2=Frame(side_bar,relief=SUNKEN,borderwidth=1)
    side_bar2.grid(column=0,row=2,sticky=NE,padx=4,pady=4,ipadx=2,ipady=2)
    Label(side_bar2,text='<<< DETAIL >>>').grid(column=0,row=0,padx=2,pady=2,columnspan=3)
    detail=('[Trading ID]','[Container ID]','[No.]')
    for i in range(len(detail)):
        Label(side_bar2,text=detail[i]).grid(column=0+i,row=1,sticky=NW,padx=4)
    for i in range(len(reefer)):
        Label(side_bar2,text=reefer[i][0]).grid(column=0,row=2+i,sticky=NW,padx=4)
        Label(side_bar2,text=reefer[i][6]).grid(column=1,row=2+i,sticky=NW,padx=4)
        Label(side_bar2,text=i+1).grid(column=2,row=2+i,sticky=NE,padx=4)
    for i in range(len(dry)):
        Label(side_bar2,text=dry[i][0]).grid(column=0,row=2+i+len(reefer),sticky=NW,padx=4)
        Label(side_bar2,text=dry[i][6]).grid(column=1,row=2+i+len(reefer),sticky=NW,padx=4)
        Label(side_bar2,text=i+1+len(reefer)).grid(column=2,row=2+i+len(reefer),sticky=NE,padx=4)
    j=0
    for i in range(len(dry)):
        j=j+dry[1][i]
        
    if(j!=1):
        showinfo(title="Message",message="About "+str(int(j))+" 40' dry containers could not be placed on ship\nbecause those left places can only fit 20' dry container.")
    j=0
    for i in range(len(reefer)):
        j=j+dry[1][i]
    if(j!=1):
        showinfo(title="Message",message="About "+str(int(j))+" 40' reefer could not be placed on ship\nbecause those left reefer places can only fit 20' reefer.")

def control_canvas_size(canvas):
    canvas.configure(scrollregion=canvas.bbox("all"))

menu_bar=Menu(tk)       # menu bar
tk.config(menu=menu_bar)
menu_button=Menu(menu_bar)
menu_bar.add_cascade(label="Menu", underline=0,menu=menu_button)
menu_inside=(('New Data','Load Data','Log Out','Quit'),('Ctrl+N','Ctrl+L','Ctrl+G','Ctrl+Q'),(newdata_interface,loaddata_interface,logout,exiting),(0,0,2,0))
for a in range(len(menu_inside[0])):
        menu_button.add_command(label=menu_inside[0][a],underline=menu_inside[3][a],command=menu_inside[2][a],accelerator=menu_inside[1][a])
        if(a==1)|(a==2):
                menu_button.add_separator()
ship_button=Menu(menu_bar)
menu_bar.add_cascade(label="Ship Data",underline=0,menu=ship_button)
ship_button.add_command(label="New Ship",command=newship)
ship_button.add_command(label="Edit...",command=editship)
ship_button.add_command(label="Delete...",command=deleteship)
del_button=Menu(menu_bar)
menu_bar.add_cascade(label="Delete...",underline=0,menu=del_button)
del_button.add_command(label="Delete Customer",command=del_cust)
del_button.add_command(label="Delete Item",command=del_item)
arrange_button=Menu(menu_bar)
menu_bar.add_cascade(label="Arrange...",underline=0,menu=arrange_button)
arrange_button.add_command(label="Illustrate Arrangement on Ship",command=arrange_system)
about_button=Menu(menu_bar)
menu_bar.add_cascade(label="Help",underline=0,menu=about_button)
about_button.add_command(label="About Software",command=version)
tk.wm_protocol('WM_DELETE_WINDOW',exiting)      #redirecting close button
tk.bind_all("<Control-q>",lambda event: exiting())
random.seed = (os.urandom(1024))
login_interface()
tk.mainloop()
connection.close()
